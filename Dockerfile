ARG UNIT_TESTS=off # See below for the description

# ----------

FROM debian:bookworm-slim AS build

RUN apt-get update && apt-get install --yes --no-install-recommends \
    cmake \
    g++ \
    git \
    libbenchmark-dev \
    libfuse3-dev \
    libgmock-dev \
    libgoogle-glog-dev \
    libgrpc++-dev \
    libgtest-dev \
    libprotobuf-dev \
    libssl-dev \
    libtomlplusplus-dev \
    ninja-build \
    nlohmann-json3-dev \
    pkg-config \
    protobuf-compiler-grpc \
    uuid-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

#
# If set to 'on' then final image will contain built unit tests instead of binaries
#
ARG UNIT_TESTS=off

#
# If set to 'on' then only RECC will be built, with support for clang-scan-deps
#
ARG ONLY_RECC_WITH_CLANG_SCAN_DEPS=off

#
# If set to 'on' then hardening compilation flags will be enabled
#
ARG HARDEN=off

#
# If set to 'on' then mostly-static build will be enabled: static libraries will be preferred to dynamic
#
ARG MOSTLY_STATIC=off

#
# If set to 'on' then code coverage collection will be enabled
#
ARG COVERAGE=off

#
# Sets CMake build type
#
ARG BUILD_TYPE=RELEASE

COPY . /buildbox

RUN \
    HARDEN=${HARDEN} \
    MOSTLY_STATIC=${MOSTLY_STATIC} \
    ONLY_RECC_WITH_CLANG_SCAN_DEPS=${ONLY_RECC_WITH_CLANG_SCAN_DEPS} \
    UNIT_TESTS=${UNIT_TESTS} \
    COVERAGE=${COVERAGE} \
    BUILD_TYPE=${BUILD_TYPE} \
    /buildbox/build-scripts/build.sh

#
# How the final container is built:
#
# If unit tests are not enabled, then it contains Buildbox binaries and
# their runtime dependencies, and is built thus:
# - image-runtime-deps + image-tests-off + image
#
# Otherwise the image contains Buildbox checkout, build directory and all
# the dependencies needed to run unit tests, and is built thus:
# - image-runtime-deps + image-tests-on + image
#

# ----------

FROM debian:bookworm-slim AS image-runtime-deps

#
# Runtime dependencies only. To make it easier to maintain this list
# only top-level dependencies are mentioned here (e.g. libgrpc++ depends
# on libprotobuf, so there is no need to specify exact libprotobuf package name)
#
RUN apt-get update && apt-get install --no-install-recommends --yes \
    bubblewrap \
    fuse3 \
    libgrpc++1.51 \
    libgoogle-glog0v6 \
    libssl3 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# ----------

FROM image-runtime-deps AS image-tests-off
ARG ONLY_RECC_WITH_CLANG_SCAN_DEPS=off

COPY --from=build /usr/local/bin/* /usr/local/bin/
COPY build-scripts/image-tests-off.sh /tmp

RUN \
    ONLY_RECC_WITH_CLANG_SCAN_DEPS=${ONLY_RECC_WITH_CLANG_SCAN_DEPS} \
     /tmp/image-tests-off.sh && rm /tmp/image-tests-off.sh

# ----------

FROM image-runtime-deps AS image-tests-on

#
# Additional packages needed for running tests
#
RUN apt-get update && apt-get install --yes --no-install-recommends \
    attr \
    cmake \
    g++ \
    libc6-dev \
    libprotobuf-dev \
    protobuf-compiler \
    net-tools \
    socat \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY --from=build /buildbox /buildbox

# ----------

FROM image-tests-${UNIT_TESTS} AS image
