#!/usr/bin/env bash
set -e

# Determine CMake flags

declare -a CMAKE_FLAGS

echo "Build configuration:"

case "$HARDEN" in
  on)
    echo " * enable hardening"
    CMAKE_FLAGS+=(-DHARDEN=ON);;
  off) ;;
  *)
    echo "Unexpected HARDEN value: $HARDEN" >&2
    exit 1;;
esac

case "$MOSTLY_STATIC" in
  on)
    echo " * mostly static build"
    CMAKE_FLAGS+=(-DBUILD_MOSTLY_STATIC=ON);;
  off) ;;
  *)
    echo "Unexpected MOSTLY_STATIC value: $MOSTLY_STATIC" >&2
    exit 1;;
esac

case "$ONLY_RECC_WITH_CLANG_SCAN_DEPS" in
  on)
    echo " * build only RECC with clang-scan-deps"
    CMAKE_FLAGS+=(-DTOOLS=OFF -DRECC=ON -DCLANG_SCAN_DEPS=ON);;
  off) ;;
  *)
    echo "Unexpected ONLY_RECC_WITH_CLANG_SCAN_DEPS value: $ONLY_RECC_WITH_CLANG_SCAN_DEPS" >&2
    exit 1;;
esac

case "$UNIT_TESTS" in
  on)
    echo " * build unit tests";;
  off)
    CMAKE_FLAGS+=(-DBUILD_TESTING=OFF);;
  *)
    echo "Unexpected UNIT_TESTS value: $UNIT_TESTS" >&2
    exit 1;;
esac

case "$COVERAGE" in
  on)
    echo " * enable coverage"
    CMAKE_FLAGS+=(-DCOVERAGE=ON);;
  off) ;;
  *)
    echo "Unexpected COVERAGE value: $COVERAGE" >&2
    exit 1;;
esac

case "$BUILD_TYPE" in
  "")
    echo "Unexpected missing BUILD_TYPE" >&2
    exit 1;;
  *)
    echo " * build type is $BUILD_TYPE"
    CMAKE_FLAGS+=(-DCMAKE_BUILD_TYPE=$BUILD_TYPE);;
esac

echo "CMake flags: ${CMAKE_FLAGS}"

# Add non-root user for devcontainer use
groupadd -r builder
useradd --shell /bin/bash -m -r -g builder builder
echo "builder ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

mkdir -p /buildbox/build
cd /buildbox/build

cmake -G Ninja ${CMAKE_FLAGS[@]} ..
ninja
if [ "$UNIT_TESTS" = off ]; then
  ninja install
fi
