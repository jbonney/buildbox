#!/bin/sh
set -e

#
# Tags may point to commits in the past, so build 'latest' only for builds
# from default branch.
#
if [ -n "$CI_COMMIT_TAG" ]; then
  TAG=$CI_COMMIT_TAG
else
  TAG=latest
fi

docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
docker build -t $CI_REGISTRY_IMAGE:$TAG .

if [ -z "$TEST" ]; then
  docker push $CI_REGISTRY_IMAGE:$TAG
fi
