option(CAS_SERVER_TESTS_USES_TCP "CAS server tests use tcp" OFF)
if (CAS_SERVER_TESTS_USES_TCP)
    add_definitions("-DUSER_TEST_PROXY_SERVER_ADDRESS=\"127.0.0.1:50055\"")
    add_definitions("-DUSER_TEST_REMOTE_SERVER_ADDRESS=\"127.0.0.1:50065\"")
else()
    add_definitions("-DUSER_TEST_PROXY_SERVER_ADDRESS=\"\"")
    add_definitions("-DUSER_TEST_REMOTE_SERVER_ADDRESS=\"\"")
endif()

file(GLOB SRCS *.cpp)

add_library(casd STATIC ${SRCS})
target_precompile_headers(casd REUSE_FROM common)
target_link_libraries(casd common commonmetrics)
target_include_directories(casd PRIVATE ".")

## Add compiler specific flags for -Werror. PRIVATE specifies to not add these flags to libraries that link against this one (in this case tests).
if(${CMAKE_SYSTEM_NAME} MATCHES "AIX" AND CMAKE_CXX_COMPILER_ID EQUAL "GNU")
    # We want to avoid setting errors on GNU + AIX because system headers + not being able to use -isystem, so no $DEBUG_FLAGS here
    target_link_options(casd PRIVATE -lsupc++)
endif()

# Before gRPC v1.30.0 a bug prevented clients from being notitied that streams
# were half-closed by the other end: https://github.com/grpc/grpc/pull/22668.
# casd closes its end of a stream when returning early from
# `ByteStream.Write()` calls when the blob is already in storage.
# In order to avoid potential side effects with buggy clients that will try and
# keep sending data to a closed stream, we only enable that early return for
# versions of gRPC that have that issue fixed.
if(gRPC_VERSION VERSION_GREATER_EQUAL 1.30.0 OR BUILDBOX_CASD_BYTESTREAM_WRITE_RETURN_EARLY)
  target_compile_definitions(casd PRIVATE BUILDBOX_CASD_BYTESTREAM_WRITE_RETURN_EARLY)
  message("Enabling BUILDBOX_CASD_BYTESTREAM_WRITE_RETURN_EARLY")
endif()

add_executable(buildbox-casd bin/buildboxcasd.m.cpp)
target_precompile_headers(buildbox-casd REUSE_FROM common)
target_link_libraries(buildbox-casd casd)
target_include_directories(buildbox-casd PRIVATE ".")
install(TARGETS buildbox-casd RUNTIME DESTINATION bin)

add_subdirectory(test)

option(CASD_BUILD_BENCHMARK "Build benchmarks" ON)
if(CASD_BUILD_BENCHMARK)
    add_subdirectory(benchmark)
endif()
