/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_ACTIONSTORAGE_H
#define INCLUDED_BUILDBOXCASD_ACTIONSTORAGE_H

#include <buildboxcommon_protos.h>

using namespace build::bazel::remote::execution::v2;
using namespace build::buildgrid;

namespace buildboxcasd {

class ActionStorage {
    /*
     * Defines a common interface for action storage polymorphism
     */
  public:
    virtual ~ActionStorage(){};
    /*
     * Reads the entry [action_digest] from the underlying storage
     * into [result]. Returns true if successful.
     */
    virtual bool readAction(const Digest &action_digest,
                            ActionResult *result) = 0;
    /*
     * Creates an entry storing [action_digest] and its
     * corresponding ActionResult [result]. Replaces the old result
     * if action_digest is already stored.
     */
    virtual void storeAction(const Digest &action_digest,
                             const ActionResult &result) = 0;

  protected: // protected so we can have a constructor in subclasses
    ActionStorage() = default;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_ACTIONSTORAGE_H
