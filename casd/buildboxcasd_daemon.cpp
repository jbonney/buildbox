/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_daemon.h>

#include <exception>
#include <fcntl.h>
#include <google/rpc/code.pb.h>
#include <grpcpp/channel.h>
#include <signal.h>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <system_error>
#include <thread>
#include <unistd.h>

#include <grpcpp/server.h>
#include <grpcpp/server_context.h>

#include <buildboxcasd_fslocalactionstorage.h>
#include <buildboxcommon_casclient.h>
#include <buildboxcommon_commandline.h>
#include <buildboxcommon_connectionoptions_commandline.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_logging_commandline.h>
#include <buildboxcommon_notify.h>

#include <buildboxcasd_fslocalcas.h>
#include <buildboxcasd_lrulocalcas.h>
#include <buildboxcasd_server.h>

namespace buildboxcasd {
namespace {

int64_t parseSize(const std::string &value)
{
    const char *s = value.c_str();
    char *endptr;
    int64_t size = strtoll(s, &endptr, 10);

    if (endptr == s || size < 0) {
        // Invalid number
        return 0;
    }
    else if (strlen(endptr) == 0) {
        // Valid number without suffix
        return size;
    }
    else if (strlen(endptr) == 1) {
        // Valid number with suffix
        switch (*endptr) {
            case 'K':
                return size * 1000ll;
            case 'M':
                return size * 1000000ll;
            case 'G':
                return size * 1000000000ll;
            case 'T':
                return size * 1000000000000ll;
            default:
                // Invalid suffix
                return 0;
        }
    }
    else {
        // Valid number with invalid suffix
        return 0;
    }
}

} // namespace

bool Daemon::configure(const buildboxcommon::CommandLine &cml,
                       const std::string &cachePath)
{
    // If no new options are specified then we use compatibility mode
    bool compatMode = !cml.exists("remote") &&
                      !cml.exists("local-server-instance") &&
                      !cml.exists("proxy-instance");

    if (!buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
            cml, "", &d_cas_server)) {
        std::cerr << "Error configuring CAS channel" << std::endl;
        return false;
    }
    if (!buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
            cml, "", &d_ac_server)) {
        std::cerr << "Error configuring ActionCache channel" << std::endl;
        return false;
    }
    if (!buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
            cml, "", &d_ra_server)) {
        std::cerr << "Error configuring RemoteAsset channel" << std::endl;
        return false;
    }
    if (!buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
            cml, "", &d_exec_server)) {
        std::cerr << "Error configuring Execution channel" << std::endl;
        return false;
    }

    if (compatMode) {
        // In compatibility mode --instance is not used for connection options,
        // undo parsing by ::configureChannel.
        d_cas_server.d_instanceName = "";
        d_ac_server.d_instanceName = "";
        d_ra_server.d_instanceName = "";
        d_exec_server.d_instanceName = "";
    }

    if (!buildboxcommon::ConnectionOptionsCommandLine::updateChannelOptions(
            cml, "cas-", &d_cas_server)) {
        std::cerr << "Error configuring CAS channel" << std::endl;
        return false;
    }

    if (!buildboxcommon::ConnectionOptionsCommandLine::updateChannelOptions(
            cml, "ac-", &d_ac_server)) {
        std::cerr << "Error configuring ActionCache channel" << std::endl;
        return false;
    }
    if (!buildboxcommon::ConnectionOptionsCommandLine::updateChannelOptions(
            cml, "ra-", &d_ra_server)) {
        std::cerr << "Error configuring Remote Asset channel" << std::endl;
        return false;
    }
    if (!buildboxcommon::ConnectionOptionsCommandLine::updateChannelOptions(
            cml, "exec-", &d_exec_server)) {
        std::cerr << "Error configuring Execution channel" << std::endl;
        return false;
    }

    bool is_proxy = d_cas_server.d_url != "" || d_ac_server.d_url != "" ||
                    d_ra_server.d_url != "" || d_exec_server.d_url != "";

    if (compatMode) {
        if (cml.exists("instance")) {
            d_instance_name = cml.getString("instance");

            std::string new_arg_name;
            if (is_proxy) {
                new_arg_name = "--proxy-instance";
            }
            else {
                new_arg_name = "--local-server-instance";
            }
            std::cerr
                << "WARNING" << std::endl
                << "WARNING --instance without --remote is treated "
                << "as " << new_arg_name << "." << std::endl
                << "WARNING This behavior is obsolete and will be removed."
                << std::endl
                << "WARNING Replace --instance with " << new_arg_name << "."
                << std::endl
                << "WARNING" << std::endl;
        }

        if (cml.exists("server-instance")) {
            std::cerr
                << "WARNING" << std::endl
                << "WARNING --server-instance is obsolete and will be removed."
                << std::endl
                << "WARNING Replace --server-instance with "
                   "--local-server-instance."
                << std::endl
                << "WARNING" << std::endl;
            d_local_server_instance_name = cml.getString("server-instance");
        }
    }
    else {
        // In new mode --server-instance is not allowed
        if (cml.exists("server-instance")) {
            std::cerr << "--server-instance is not available with --remote "
                         "and other new options."
                      << std::endl
                      << "Replace it with --local-server-instance."
                      << std::endl;
            return false;
        }

        if (is_proxy) {
            if (cml.exists("proxy-instance")) {
                d_instance_name = cml.getString("proxy-instance");
            }
            d_local_server_instance_name =
                cml.getString("local-server-instance", "");
        }
        else {
            if (cml.exists("proxy-instance")) {
                std::cerr << "--proxy-instance cannot be used if no remotes "
                             "are specified,"
                          << std::endl
                          << "as buildbox-casd is not running in proxy mode."
                          << std::endl;
                return false;
            }
            d_instance_name = cml.getString("local-server-instance", "");
        }
    }

    if (cml.exists("bind")) {
        d_bind_address = cml.getString("bind");
    }

    if (cml.exists("buildbox-run")) {
        d_runnerCommand = cml.getString("buildbox-run");
        d_extraRunArgs = cml.getVS("runner-arg");
        d_maxJobs = cml.getInt("jobs");
        if (d_maxJobs < 1) {
            std::cerr << "Invalid value for --jobs " << d_maxJobs << "\n";
        }
    }

    d_quota_high = parseSize(cml.getString("quota-high", ""));
    d_quota_low = parseSize(cml.getString("quota-low", ""));
    if (d_quota_high == 0 && d_quota_low > 0) {
        std::cerr << "--quota-low is supported only in combination with "
                     "--quota-high\n";
        return false;
    }

    if (d_quota_high > 0 && d_quota_low == 0) {
        // Default the low quota to half of the high quota
        d_quota_low = d_quota_high / 2;
    }

    if (cml.exists("read-only-remote")) {
        d_read_only_remote = true;
    }
    else {
        d_read_only_remote = false;
    }
    d_reserved_space = parseSize(cml.getString("reserved"));
    d_protect_session_blobs = cml.getBool("protect-session-blobs");
    d_proxy_findmissingblobs_cache_ttl_seconds =
        cml.getInt("findmissingblobs-cache-ttl");

    buildboxcommon::LogLevel logLevel;
    if (!buildboxcommon::parseLoggingOptions(cml, logLevel)) {
        return false;
    }
    BUILDBOX_LOG_SET_LEVEL(logLevel);
    d_log_level = logLevel;

    d_local_cache_path = cachePath;
    if (d_local_cache_path.empty()) {
        std::cerr << "Local cache path is missing" << std::endl;
        return false;
    }

    d_allow_external_file_moves = cml.getBool("capture-allow-file-moves");

    if (!buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
            cml, "ac-", &d_ac_server)) {
        std::cerr << "Error configuring ActionCache channel" << std::endl;
        return false;
    }

    return true;
} // namespace buildboxcasd

void Daemon::logCommandLine() const
{
    std::ostringstream oss;
    oss << "Starting CASd instance \"" << this->d_instance_name
        << "\" with cache at \"" << this->d_local_cache_path << "\", "
        << "instance_name = \"" << d_instance_name << "\", bind = \""
        << d_bind_address << "\", quota-high = \"" << d_quota_high
        << "\", quota-low = \"" << d_quota_low << "\", reserved = \""
        << d_reserved_space << "\", protect_session_blobs = " << std::boolalpha
        << d_protect_session_blobs << ", findmissingblobs-cache-ttl = "
        << d_proxy_findmissingblobs_cache_ttl_seconds << ", log-level = \""
        << d_log_level << "\", CAS-client = [" << d_cas_server
        << "], Remote-Asset-client = [" << d_ra_server
        << "], ActionCache-client = [" << d_ac_server
        << "], Execution-client = [" << d_exec_server << "]";

    BUILDBOX_LOG_INFO(oss.str());
}

void Daemon::runDaemon()
{
    logCommandLine();

    const std::string unix_prefix = "unix:";
    if (d_bind_address.empty()) {
        const auto path = (d_local_cache_path.back() == '/')
                              ? d_local_cache_path
                              : d_local_cache_path + "/";

        d_bind_address = unix_prefix + path + "casd.sock";
    }

    // Creating an FsLocalCas instance:
    std::unique_ptr<LocalCas> fs_storage = std::make_unique<FsLocalCas>(
        this->d_local_cache_path, this->d_allow_external_file_moves);

    // Creating an FsLocalActionStorage instance:
    std::shared_ptr<FsLocalActionStorage> actioncache_storage =
        std::make_shared<FsLocalActionStorage>(this->d_local_cache_path);

    std::shared_ptr<LocalCas> storage;

    if (d_quota_high == 0) {
        // No quota configured.
        // LRU expiry only if there is insufficient disk space.
        d_quota_high = INT64_MAX;
        d_quota_low = d_quota_high / 2;
    }

    // Creating LocalCas wrapper for LRU expiry
    auto lru_storage = std::make_shared<LruLocalCas>(
        std::move(fs_storage), d_quota_low, d_quota_high, d_reserved_space);

    if (d_protect_session_blobs) {
        // Do not delete blobs created or used in this session
        lru_storage->protectActiveBlobs(std::chrono::system_clock::now());
    }

    storage = lru_storage;

    auto asset_storage =
        std::make_shared<FsLocalAssetStorage>(this->d_local_cache_path);

    if (!d_cas_server.d_url.empty()) { // CAS/RA proxy
        BUILDBOX_LOG_INFO("Creating CAS proxy server, remote CAS address = \""
                          << d_cas_server.d_url << "\"");

        const buildboxcommon::ConnectionOptions *ra_endpoint = nullptr;
        if (!d_ra_server.d_url.empty()) {
            BUILDBOX_LOG_INFO("Creating Remote Asset proxy server, Remote "
                              "Asset server address = \""
                              << d_ra_server.d_url << "\"");
            ra_endpoint = &d_ra_server;
        }

        const buildboxcommon::ConnectionOptions *ac_endpoint = nullptr;
        if (!d_ac_server.d_url.empty()) { // ActionCache proxy
            BUILDBOX_LOG_INFO("Creating ActionCache proxy server, cache "
                              "server address = \""
                              << d_ac_server.d_url << "\"");
            ac_endpoint = &d_ac_server;
        }

        const buildboxcommon::ConnectionOptions *execution_endpoint = nullptr;
        if (!d_exec_server.d_url.empty()) { // Execution proxy
            BUILDBOX_LOG_INFO("Creating Execution proxy server, execution "
                              "server address = \""
                              << d_exec_server.d_url << "\"");
            execution_endpoint = &d_exec_server;
        }

        d_server = std::make_shared<Server>(
            storage, asset_storage, actioncache_storage, d_cas_server,
            ra_endpoint, ac_endpoint, execution_endpoint,
            this->d_instance_name, d_read_only_remote,
            d_proxy_findmissingblobs_cache_ttl_seconds);
    }
    else { // Local CAS/RA server
        if (!d_ra_server.d_url.empty()) {
            BUILDBOX_LOG_ERROR("Cannot create Remote Asset proxy without "
                               "remote CAS server");
            exit(1);
        }
        if (!d_ac_server.d_url.empty()) {
            BUILDBOX_LOG_ERROR("Cannot create ActionCache proxy without "
                               "remote CAS server");
            exit(1);
        }

        BUILDBOX_LOG_DEBUG(
            "Creating local CAS, ActionCache, and Remote Asset servers");

        d_server = std::make_shared<Server>(
            storage, asset_storage, actioncache_storage, this->d_instance_name,
            this->d_bind_address, this->d_runnerCommand, this->d_extraRunArgs,
            this->d_maxJobs);
    }

    // Optionally, add an additional local server instance:
    if (!d_local_server_instance_name.empty()) {
        BUILDBOX_LOG_INFO("Creating local server instance with name \""
                          << d_local_server_instance_name << "\"");
        d_server->addLocalServerInstance(
            d_local_server_instance_name, this->d_instance_name,
            this->d_bind_address, this->d_runnerCommand, this->d_extraRunArgs,
            this->d_maxJobs);
    }

    const std::string tmp_suffix = ".tmp";
    if (d_bind_address.find(unix_prefix) == 0) {
        // Use temporary socket path during initialization to avoid
        // exposing uninitialized socket to other processes. This allows
        // race-free connections from client processes.
        d_socket_path = d_bind_address.substr(unix_prefix.size());
        d_bind_address += tmp_suffix;
    }

    d_server->addListeningPort(d_bind_address);

    // Ignore SIGPIPE in case of using sockets + grpc without MSG_NOSIGNAL
    // support configured
    struct sigaction new_sig;
    new_sig.sa_handler = SIG_IGN;
    new_sig.sa_flags = 0;
    sigfillset(&new_sig.sa_mask);
    if (sigaction(SIGPIPE, &new_sig, NULL) == -1) {
        BUILDBOX_LOG_ERROR("Failed to set SIGPIPE handler");
        exit(1);
    }

    d_server->start();

    if (!d_socket_path.empty()) {
        // Socket has been fully initialized. Rename socket to final path.
        rename((d_socket_path + tmp_suffix).c_str(), d_socket_path.c_str());
        d_bind_address = unix_prefix + d_socket_path;
    }

    buildboxcommon::systemd_notify_socket_send_ready();

    BUILDBOX_LOG_INFO("Server listening on " << d_bind_address);
}

void Daemon::stop()
{
    // Use local variable to avoid race condition with nullptr check
    std::shared_ptr<Server> server = d_server;

    if (server != nullptr) {
        server->shutdown();
        server->wait();
        d_server = nullptr;
    }

    d_shutdown = true;

    if (!d_socket_path.empty()) {
        // Clean up socket. This is not handled by gRPC as we use a
        // temporary path during initialization.
        unlink(d_socket_path.c_str());
    }
}

} // namespace buildboxcasd
