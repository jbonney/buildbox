/*
 * Copyright 2023 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_EXECUTIONINSTANCE_H
#define INCLUDED_BUILDBOXCASD_EXECUTIONINSTANCE_H

#include <buildboxcommon_protos.h>

#include <string>

using namespace build::bazel::remote::execution::v2;
using namespace build::buildgrid;
using namespace google::longrunning;

using grpc::ServerContext;
using grpc::ServerWriterInterface;
using grpc::Status;

namespace buildboxcasd {

class ExecutionInstance {
    /* Defines a common interface for providing the necessary methods to
     * service the calls of the Execution service portion of the Remote
     * Execution API.
     */
  public:
    ExecutionInstance(const std::string &instance_name);
    virtual ~ExecutionInstance() = 0;
    virtual grpc::Status Execute(ServerContext *ctx,
                                 const ExecuteRequest &request,
                                 ServerWriterInterface<Operation> *writer);

    virtual grpc::Status
    WaitExecution(ServerContext *ctx, const WaitExecutionRequest &request,
                  ServerWriterInterface<Operation> *writer);

    virtual grpc::Status GetOperation(const GetOperationRequest &request,
                                      Operation *response);

    virtual grpc::Status ListOperations(const ListOperationsRequest &request,
                                        ListOperationsResponse *response);

    virtual grpc::Status CancelOperation(const CancelOperationRequest &request,
                                         google::protobuf::Empty *response);

    virtual void stop();

    std::string instanceName() const { return d_instance_name; }

  protected:
    const std::string d_instance_name;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_EXECUTIONINSTANCE_H
