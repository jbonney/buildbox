/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_instancemanager.h>

namespace buildboxcasd {

InstanceManager::InstanceManager() {}

bool InstanceManager::addInstance(
    const std::string &instance_name,
    std::shared_ptr<CasInstance> cas_instance,
    std::shared_ptr<RaInstance> ra_instance,
    std::shared_ptr<AcInstance> ac_instance,
    std::shared_ptr<ExecutionInstance> execution_instance)
{
    std::lock_guard<std::mutex> lock(d_instance_map_mutex);

    if (d_instance_map.find(instance_name) != d_instance_map.end()) {
        return false;
    }

    Instance instance;
    instance.casInstance = cas_instance;
    instance.raInstance = ra_instance;
    instance.acInstance = ac_instance;
    instance.executionInstance = execution_instance;

    d_instance_map[instance_name] = std::move(instance);
    return true;
}

std::shared_ptr<CasInstance>
InstanceManager::getCasInstance(const std::string &instance_name)
{
    std::lock_guard<std::mutex> lock(d_instance_map_mutex);

    auto it = d_instance_map.find(instance_name);
    if (it == d_instance_map.end()) {
        return nullptr;
    }

    return it->second.casInstance;
}

std::shared_ptr<RaInstance>
InstanceManager::getRaInstance(const std::string &instance_name)
{
    std::lock_guard<std::mutex> lock(d_instance_map_mutex);

    auto it = d_instance_map.find(instance_name);
    if (it == d_instance_map.end()) {
        return nullptr;
    }

    return it->second.raInstance;
}

std::shared_ptr<AcInstance>
InstanceManager::getAcInstance(const std::string &instance_name)
{
    std::lock_guard<std::mutex> lock(d_instance_map_mutex);

    auto it = d_instance_map.find(instance_name);
    if (it == d_instance_map.end()) {
        return nullptr;
    }

    return it->second.acInstance;
}

std::shared_ptr<ExecutionInstance>
InstanceManager::getExecutionInstance(const std::string &instance_name)
{
    std::lock_guard<std::mutex> lock(d_instance_map_mutex);

    auto it = d_instance_map.find(instance_name);
    if (it == d_instance_map.end()) {
        return nullptr;
    }

    return it->second.executionInstance;
}

bool InstanceManager::contains(const std::string &instance_name)
{
    std::lock_guard<std::mutex> lock(d_instance_map_mutex);

    auto it = d_instance_map.find(instance_name);
    return it != d_instance_map.end();
}

void InstanceManager::stopExecutions()
{
    std::lock_guard<std::mutex> lock(d_instance_map_mutex);

    for (auto instance : d_instance_map) {
        if (instance.second.executionInstance != nullptr) {
            instance.second.executionInstance->stop();
        }
    }
}

} // namespace buildboxcasd
