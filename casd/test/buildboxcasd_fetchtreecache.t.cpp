/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_fetchtreecache.h>

#include <buildboxcommon_protos.h>

#include <gtest/gtest.h>

using buildboxcommon::Digest;

class FetchTreeCacheFixture : public testing::Test {
  protected:
    FetchTreeCacheFixture()
    {
        digest.set_hash("hashABC");
        digest.set_size_bytes(1234);
    }

    buildboxcasd::FetchTreeCache cache;
    Digest digest;
};

TEST_F(FetchTreeCacheFixture, EmptyCache)
{
    ASSERT_FALSE(cache.hasRootDigest(digest, false));
    ASSERT_FALSE(cache.hasRootDigest(digest, true));
}

TEST_F(FetchTreeCacheFixture, CacheNoFiles)
{
    ASSERT_FALSE(cache.hasRootDigest(digest, false));
    ASSERT_FALSE(cache.hasRootDigest(digest, true));

    cache.addRootDigest(digest, false);

    EXPECT_TRUE(cache.hasRootDigest(digest, false));
    EXPECT_FALSE(cache.hasRootDigest(digest, true));
}

TEST_F(FetchTreeCacheFixture, CacheWithFiles)
{
    ASSERT_FALSE(cache.hasRootDigest(digest, false));

    cache.addRootDigest(digest, true);

    EXPECT_TRUE(cache.hasRootDigest(digest, false));
    EXPECT_TRUE(cache.hasRootDigest(digest, true));
}
