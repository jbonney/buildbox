# casdownload

`casdownload` facilitates downloading blobs, action outputs and directory trees
from a previously configured Content Addressable Store and Action Cache (such
as BuildGrid, Buildbarn or Buildfarm).

## Usage

```
Usage: ./casdownload
   --help                      Display usage end exit
   --remote                    URL for the CAS service [optional]
   --instance                  Name of the CAS instance [optional, default = ""]
   --server-cert               Public server certificate for CAS TLS (PEM-encoded) [optional]
   --client-key                Private client key for CAS TLS (PEM-encoded) [optional]
   --client-cert               Private client certificate for CAS TLS (PEM-encoded) [optional]
   --access-token              Access Token for authentication CAS (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorization bearer token [optional]
   --token-reload-interval     How long to wait before refreshing access token [optional]
   --googleapi-auth            Use GoogleAPIAuth for CAS service [optional, default = false]
   --retry-limit               Number of times to retry on grpc errors for CAS service [optional, default = "4"]
   --retry-delay               How long to wait in milliseconds before the first grpc retry for CAS service [optional, default = "1000"]
   --request-timeout           Sets the timeout for gRPC requests in seconds. Set to 0 for no timeout. [optional, default = "0"]
   --min-throughput            Sets the minimum throughput for gRPC requests in bytes per seconds.
                                  The value may be suffixed with K, M, G or T. [optional, default = "0"]
   --keepalive-time            Sets the period for gRPC keepalive pings in seconds. Set to 0 to disable keepalive pings. [optional, default = "0"]
   --load-balancing-policy     Which grpc load balancing policy to use for CAS service.
                                  Valid options are 'round_robin' and 'grpclb' [optional]
   --destination-dir           Directory to save downloaded data [required]
   --root-digest               Download a tree by its root ID [optional]
   --action-digest             Download the stderr, stdout, output files and directories of this digest via the ActionCache [optional]
   --file-digest               Download a blob by its content [optional]
   --cas-server                [Deprecated] Use --remote [optional]
   --log-level                 Log level [optional, default = "error"]
   --verbose                   Set log level to 'debug' [optional]

Download the specified digest(root/action/file) into the specified directory

Action digest download expects that ActionCache is hosted on the same
server as the CAS server.

Example usage:
  casdownload --remote=https://localhost:50051
              --destination=/path/to/output
              --root-digesh=deafbeef/123
```

### Examples with Docker

`casdownload` can be used via the [`Dockerfile`](Dockerfile). The following
illustrates the `file-digest` mode to download a single blob from the CAS:

```
$ cd cpp/casdownload
$ docker build . -t casdownload
...
$ docker run -it casdownload bash
$ casdownload --instance=dev --remote=http://my-cas-server:60051 --file-digest=9fca6f98a3b46af41101fca85842fc52eb0cee16abb92dc4be6aee87afd4994d/142 --destination-dir=dir
CAS client connecting to http://my-cas-server:60051
Starting to download 9fca6f98a3b46af41101fca85842fc52eb0cee16abb92dc4be6aee87afd4994d/142 to "dir"
Downloaded blob to dir/blob
Finished downloading 9fca6f98a3b46af41101fca85842fc52eb0cee16abb92dc4be6aee87afd4994d/142 to "dir" in 0.036 second(s)
$ cat dir/blob
Hello, world!
$
```

To download an Action:

```
$ casdownload --instance=dev --remote=http://my-cas-server:40051 --action-digest=0125ae312e4743af312bc6107558ecb1d4dc79eeb8c2314285c28f825ccede5e/142 --destination-dir=actionresult
CAS client connecting to http://my-cas-server:40051
Starting to download 0125ae312e4743af312bc6107558ecb1d4dc79eeb8c2314285c28f825ccede5e/142 to "actionresult"
Finished downloading 0125ae312e4743af312bc6107558ecb1d4dc79eeb8c2314285c28f825ccede5e/142 to "actionresult" in 1.485 second(s)
$ cat actionresult/stdout
Hello, world stdout!
$ cat actionresult/stderr
Hello, world stderr!
$ ls actionresult
myoutputfile
stderr
stdout
$
```
