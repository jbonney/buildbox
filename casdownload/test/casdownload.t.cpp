#include <gtest/gtest.h>
#include <iostream>

#include <processargs.h>

TEST(CasDownloadArgs, HelpTest)
{
    const char *argv[] = {"casdownload", "--help"};
    auto args = casdownload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                         const_cast<char **>(argv));
    ASSERT_FALSE(args.d_valid);
}

TEST(CasDownloadArgs, NoRemoteTest)
{
    // Either --remote or --cas-server are required
    const char *argv[] = {"casdownload"};
    auto args = casdownload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                         const_cast<char **>(argv));
    ASSERT_FALSE(args.d_valid);
}

TEST(CasDownloadTest, DefaultsAndRootDigestTest)
{
    const char *argv[] = {"casdownload", "--remote=http://a",
                          "--destination-dir=/foo",
                          "--root-digest=deadbeef/123"};
    auto args = casdownload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                         const_cast<char **>(argv));

    ASSERT_TRUE(args.d_valid);
    ASSERT_EQ(args.d_logLevel, buildboxcommon::LogLevel::ERROR);
    ASSERT_EQ(args.d_casConnectionOptions.d_url, "http://a");
    ASSERT_EQ(args.d_destinationDir, "/foo");
    ASSERT_EQ(args.d_digestType, casdownload::DIGEST_ROOT);
    ASSERT_EQ(args.d_digest.hash(), "deadbeef");
    ASSERT_EQ(args.d_digest.size_bytes(), 123);
}

TEST(CasDownloadTest, ActionDigestTest)
{
    const char *argv[] = {"casdownload", "--remote=http://a",
                          "--destination-dir=/foo",
                          "--action-digest=deadbeef/123"};
    auto args = casdownload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                         const_cast<char **>(argv));

    ASSERT_TRUE(args.d_valid);
    ASSERT_EQ(args.d_digestType, casdownload::DIGEST_ACTION);
    ASSERT_EQ(args.d_digest.hash(), "deadbeef");
    ASSERT_EQ(args.d_digest.size_bytes(), 123);
}

TEST(CasDownloadTest, FileDigestTest)
{
    const char *argv[] = {"casdownload", "--remote=http://a",
                          "--destination-dir=/foo",
                          "--file-digest=deadbeef/123"};
    auto args = casdownload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                         const_cast<char **>(argv));

    ASSERT_TRUE(args.d_valid);
    ASSERT_EQ(args.d_digestType, casdownload::DIGEST_FILE);
    ASSERT_EQ(args.d_digest.hash(), "deadbeef");
    ASSERT_EQ(args.d_digest.size_bytes(), 123);
}

TEST(CasDownloadArgs, LogLevelTest)
{
    const char *argv[] = {"casdownload", "--log-level=warning",
                          "--remote=http://a", "--destination-dir=/foo",
                          "--root-digest=deadbeef/123"};
    auto args = casdownload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                         const_cast<char **>(argv));
    ASSERT_TRUE(args.d_valid);
    ASSERT_EQ(args.d_logLevel, buildboxcommon::LogLevel::WARNING);
}

TEST(CasDownloadArgs, VerboseTest)
{
    const char *argv[] = {"casdownload", "--verbose", "--remote=http://a",
                          "--destination-dir=/foo",
                          "--root-digest=deadbeef/123"};
    auto args = casdownload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                         const_cast<char **>(argv));
    ASSERT_TRUE(args.d_valid);
    ASSERT_EQ(args.d_logLevel, buildboxcommon::LogLevel::DEBUG);
}
