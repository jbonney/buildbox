// Copyright 2019 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <buildboxcommon_casclient.h>
#include <buildboxcommon_commandline.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_merklize.h>
#include <processargs.h>

#include <cstdlib>
#include <cstring>
#include <fstream>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#include <iostream>
#include <stdexcept>
#include <sys/stat.h>

void uploadResources(
    const buildboxcommon::digest_string_map &blobs,
    const buildboxcommon::digest_string_map &digest_to_filepaths,
    const std::unique_ptr<buildboxcommon::CASClient> &casClient)
{
    std::vector<buildboxcommon::Digest> digestsToUpload;
    for (const auto &i : blobs) {
        digestsToUpload.push_back(i.first);
    }
    for (const auto &i : digest_to_filepaths) {
        digestsToUpload.push_back(i.first);
    }

    const auto missingDigests = casClient->findMissingBlobs(digestsToUpload);

    std::vector<buildboxcommon::CASClient::UploadRequest> upload_requests;
    upload_requests.reserve(missingDigests.size());
    for (const auto &digest : missingDigests) {
        // Finding the data in one of the source maps:
        if (blobs.count(digest)) {
            upload_requests.emplace_back(
                buildboxcommon::CASClient::UploadRequest(digest,
                                                         blobs.at(digest)));
        }
        else if (digest_to_filepaths.count(digest)) {
            upload_requests.emplace_back(
                buildboxcommon::CASClient::UploadRequest::from_path(
                    digest, digest_to_filepaths.at(digest)));
        }
        else {
            throw std::runtime_error(
                "FindMissingBlobs returned non-existent digest");
        }
    }

    casClient->uploadBlobs(upload_requests);
}

void uploadDirectory(
    const std::string &path, const buildboxcommon::Digest &digest,
    const buildboxcommon::digest_string_map &directoryBlobs,
    const buildboxcommon::digest_string_map &directoryDigestToFilepaths,
    const std::unique_ptr<buildboxcommon::CASClient> &casClient)
{
    assert(casClient != nullptr);

    try {
        BUILDBOX_LOG_DEBUG("Starting to upload merkle tree...");
        uploadResources(directoryBlobs, directoryDigestToFilepaths, casClient);
        BUILDBOX_LOG_INFO("Uploaded \"" << path << "\": " << digest.hash()
                                        << "/" << digest.size_bytes());
    }
    catch (const std::runtime_error &e) {
        BUILDBOX_LOG_ERROR("Uploading " << path
                                        << " failed with error: " << e.what());
        exit(1);
    }
}

buildboxcommon::Digest processDirectory(
    const std::string &path, const bool followSymlinks,
    const std::unique_ptr<buildboxcommon::CASClient> &casClient,
    const std::shared_ptr<std::vector<buildboxcommon::IgnorePattern>>
        &ignorePatterns)
{
    buildboxcommon::digest_string_map directoryBlobs;
    buildboxcommon::digest_string_map directoryDigestToFilepaths;

    const auto ignoreMatcher =
        ignorePatterns == nullptr
            ? nullptr
            : std::make_shared<buildboxcommon::IgnoreMatcher>(path,
                                                              ignorePatterns);

    const auto singleNestedDirectory = buildboxcommon::make_nesteddirectory(
        path.c_str(), &directoryDigestToFilepaths, followSymlinks,
        ignoreMatcher);
    const auto digest = singleNestedDirectory.to_digest(&directoryBlobs);

    BUILDBOX_LOG_DEBUG("Finished building nested directory from \""
                       << path << "\": " << digest.hash() << "/"
                       << digest.size_bytes());
    BUILDBOX_LOG_DEBUG(singleNestedDirectory);

    if (casClient == nullptr) {
        BUILDBOX_LOG_INFO("Computed directory digest for \""
                          << path << "\": " << digest.hash() << "/"
                          << digest.size_bytes());
    }
    else {
        uploadDirectory(path, digest, directoryBlobs,
                        directoryDigestToFilepaths, casClient);
    }

    return digest;
}

std::unique_ptr<buildboxcommon::Digest>
processPath(const std::string &path, const bool followSymlinks,
            buildboxcommon::NestedDirectory *nestedDirectory,
            buildboxcommon::digest_string_map *digestToFilePaths,
            const std::unique_ptr<buildboxcommon::CASClient> &casClient,
            const std::shared_ptr<std::vector<buildboxcommon::IgnorePattern>>
                &ignorePatterns)
{
    BUILDBOX_LOG_DEBUG("Starting to process \""
                       << path << "\", followSymlinks = " << std::boolalpha
                       << followSymlinks << std::noboolalpha);

    struct stat statResult;
    int statFlags = 0;
    if (!followSymlinks) {
        statFlags = AT_SYMLINK_NOFOLLOW;
    }
    if (fstatat(AT_FDCWD, path.c_str(), &statResult, statFlags) != 0) {
        BUILDBOX_LOG_ERROR("Error getting file status for path \""
                           << path << "\": " << strerror(errno));
        exit(1);
    }

    if (S_ISDIR(statResult.st_mode)) {
        return std::make_unique<buildboxcommon::Digest>(
            processDirectory(path, followSymlinks, casClient, ignorePatterns));
    }
    else if (S_ISLNK(statResult.st_mode)) {
        std::string target(static_cast<size_t>(statResult.st_size), '\0');

        if (readlink(path.c_str(), &target[0], target.size()) < 0) {
            BUILDBOX_LOG_ERROR("Error reading target of symbolic link \""
                               << path << "\": " << strerror(errno));
            exit(1);
        }
        nestedDirectory->addSymlink(target, path.c_str());
    }
    else if (S_ISREG(statResult.st_mode)) {
        const buildboxcommon::File file(path.c_str());
        nestedDirectory->add(file, path.c_str());
        digestToFilePaths->emplace(file.d_digest, path);
    }
    else {
        BUILDBOX_LOG_DEBUG("Encountered unsupported file \""
                           << path << "\", skipping...");
    }

    return nullptr;
}

void writeDigestToFile(const buildboxcommon::Digest &digest,
                       const std::string &path)
{
    std::ofstream digest_file;
    digest_file.open(path);
    digest_file << digest.hash() << "/" << digest.size_bytes();
    digest_file.close();
}

int main(int argc, char *argv[])
{
    buildboxcommon::logging::Logger::getLoggerInstance().initialize(argv[0]);

    auto args = casupload::processArgs(argc, argv);
    if (args.d_processed) {
        return 0;
    }
    if (!args.d_valid) {
        return 2;
    }

    BUILDBOX_LOG_SET_LEVEL(args.d_logLevel);

    if (!args.d_outputDigestFile.empty() && args.d_paths.size() > 1) {
        // Making sure that we'll produce a single directory digest:
        const auto directoryPath = std::find_if(
            args.d_paths.cbegin(), args.d_paths.cend(),
            [args](const std::string &path) {
                return args.d_followSymlinks
                           ? buildboxcommon::FileUtils::isDirectory(
                                 path.c_str())
                           : buildboxcommon::FileUtils::isDirectoryNoFollow(
                                 path.c_str());
            });

        if (directoryPath != args.d_paths.cend()) {
            BUILDBOX_LOG_ERROR("`--output-digest-file` can be used only when "
                               "uploading a single directory structure");
            return 2;
        }
    }

    std::shared_ptr<std::vector<buildboxcommon::IgnorePattern>> ignorePatterns;
    if (args.d_ignoreFile != "") {
        std::ifstream ifs(args.d_ignoreFile);
        if (!ifs.good()) {
            BUILDBOX_LOG_ERROR(
                "Unable to read from file specified in --ignore-file");
            return 2;
        }
        ignorePatterns =
            buildboxcommon::IgnoreMatcher::parseIgnorePatterns(ifs);
    }

    // CAS client object (we don't initialize it if `dryRunMode` is set):
    std::unique_ptr<buildboxcommon::CASClient> casClient;

    try {
        if (!args.d_dryRunMode) {
            auto grpcClient = std::make_shared<buildboxcommon::GrpcClient>();
            grpcClient->init(args.d_casConnectionOptions);
            casClient =
                std::make_unique<buildboxcommon::CASClient>(grpcClient);
            casClient->init();
        }
    }
    catch (std::runtime_error &e) {
        std::cerr << "Failed to connect to CAS server: " << e.what()
                  << std::endl;
        return 1;
    }

    // Digest written to a file when `--ignore-output-digest` is specified:
    buildboxcommon::Digest outputDigest;

    buildboxcommon::NestedDirectory nestedDirectory;
    buildboxcommon::digest_string_map digestToFilePaths;

    // Upload directories individually, and aggregate files to upload as single
    // merkle tree
    std::vector<buildboxcommon::Digest> directory_digests;
    for (const auto &path : args.d_paths) {
        std::unique_ptr<buildboxcommon::Digest> dir_digest =
            processPath(path, args.d_followSymlinks, &nestedDirectory,
                        &digestToFilePaths, casClient, ignorePatterns);

        if (args.d_paths.size() == 1 && dir_digest != nullptr) {
            // Invoked for a single directory. If requested, its digest will be
            // written to `output_digest_file`.
            outputDigest = std::move(*dir_digest);
        }
    }

    if (!digestToFilePaths.empty()) {
        BUILDBOX_LOG_DEBUG("Building nested directory structure...");
        buildboxcommon::digest_string_map blobs;
        const auto directoryDigest = nestedDirectory.to_digest(&blobs);

        BUILDBOX_LOG_INFO("Computed directory digest: "
                          << directoryDigest.hash() << "/"
                          << directoryDigest.size_bytes());

        if (!args.d_dryRunMode) {
            try {
                uploadResources(blobs, digestToFilePaths, casClient);
                BUILDBOX_LOG_DEBUG("Files uploaded successfully");
                outputDigest = directoryDigest;
            }
            catch (const std::runtime_error &e) {
                BUILDBOX_LOG_ERROR(
                    "Uploading files failed with error: " << e.what());
                return 1;
            }
        }
    }

    if (!args.d_outputDigestFile.empty()) {
        BUILDBOX_LOG_DEBUG("Writing digest [" << outputDigest << "] to '"
                                              << args.d_outputDigestFile
                                              << "'");
        writeDigestToFile(outputDigest, args.d_outputDigestFile);
    }

    return 0;
}
