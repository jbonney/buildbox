#include <gtest/gtest.h>
#include <iostream>

#include <processargs.h>

TEST(CasuploadArgs, HelpTest)
{
    const char *argv[] = {"casupload", "--help"};
    auto args = casupload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                       const_cast<char **>(argv));
    ASSERT_FALSE(args.d_valid);
}

TEST(CasuploadArgs, NoRemoteTest)
{
    // Either --remote or --cas-server are required
    const char *argv[] = {"casupload", "a.txt", "b.txt"};
    auto args = casupload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                       const_cast<char **>(argv));
    ASSERT_FALSE(args.d_valid);
}

TEST(CasuploadArgs, DefaultsTest)
{
    const char *argv[] = {"casupload", "--remote=http://a", "a.txt", "b.txt"};
    auto args = casupload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                       const_cast<char **>(argv));
    ASSERT_TRUE(args.d_valid);
    ASSERT_FALSE(args.d_dryRunMode);
    ASSERT_EQ(args.d_casConnectionOptions.d_url, "http://a");
    ASSERT_EQ(args.d_logLevel, buildboxcommon::LogLevel::ERROR);
    ASSERT_FALSE(args.d_followSymlinks);
    ASSERT_EQ(args.d_paths.size(), 2);
    ASSERT_EQ(args.d_paths[0], "a.txt");
    ASSERT_EQ(args.d_paths[1], "b.txt");
    ASSERT_EQ(args.d_ignoreFile, "");
    ASSERT_EQ(args.d_outputDigestFile, "");
}

TEST(CasuploadArgs, CasServerFallback)
{
    const char *argv[] = {"casupload", "--cas-server=http://b", "a.txt"};
    auto args = casupload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                       const_cast<char **>(argv));
    ASSERT_TRUE(args.d_valid);
    ASSERT_EQ(args.d_casConnectionOptions.d_url, "http://b");
}

TEST(CasuploadArgs, RemoteOverridesCasServer)
{
    const char *argv[] = {"casupload", "--remote=http://a",
                          "--cas-server=http://b", "a.txt"};
    auto args = casupload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                       const_cast<char **>(argv));
    ASSERT_TRUE(args.d_valid);
    ASSERT_EQ(args.d_casConnectionOptions.d_url, "http://a");
}

TEST(CasuploadArgs, FollowSymlinksTest)
{
    const char *argv[] = {"casupload", "--remote=http://a",
                          "--follow-symlinks", "a.txt"};
    auto args = casupload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                       const_cast<char **>(argv));
    ASSERT_TRUE(args.d_valid);
    ASSERT_TRUE(args.d_followSymlinks);
}

TEST(CasuploadArgs, DryRunModeTest)
{
    const char *argv[] = {"casupload", "--dry-run", "--remote=http://a",
                          "a.txt"};
    auto args = casupload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                       const_cast<char **>(argv));
    ASSERT_TRUE(args.d_valid);
    ASSERT_TRUE(args.d_dryRunMode);
}

TEST(CasuploadArgs, OutputDigestTest)
{
    const char *argv[] = {"casupload", "--output-digest-file=FILE",
                          "--remote=http://a", "a.txt"};
    auto args = casupload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                       const_cast<char **>(argv));
    ASSERT_TRUE(args.d_valid);
    ASSERT_EQ(args.d_outputDigestFile, "FILE");
}

TEST(CasuploadArgs, IgnoreFileTest)
{
    const char *argv[] = {"casupload", "--ignore-file=ignorefile",
                          "--remote=http://a", "a.txt"};
    auto args = casupload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                       const_cast<char **>(argv));
    ASSERT_TRUE(args.d_valid);
    ASSERT_EQ(args.d_ignoreFile, "ignorefile");
}

TEST(CasuploadArgs, LogLevelTest)
{
    const char *argv[] = {"casupload", "--log-level=warning",
                          "--remote=http://a", "a.txt"};
    auto args = casupload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                       const_cast<char **>(argv));
    ASSERT_TRUE(args.d_valid);
    ASSERT_EQ(args.d_logLevel, buildboxcommon::LogLevel::WARNING);
}

TEST(CasuploadArgs, VerboseTest)
{
    const char *argv[] = {"casupload", "--verbose", "--remote=http://a",
                          "a.txt"};
    auto args = casupload::processArgs(sizeof(argv) / sizeof(argv[0]),
                                       const_cast<char **>(argv));
    ASSERT_TRUE(args.d_valid);
    ASSERT_EQ(args.d_logLevel, buildboxcommon::LogLevel::DEBUG);
}
