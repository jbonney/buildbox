include(CMakeFindDependencyMacro)
find_package(OpenSSL REQUIRED)
set(OPENSSL_TARGET OpenSSL::Crypto)

# Prefer protobuf's package configuration file
# https://gitlab.kitware.com/cmake/cmake/-/issues/24321
# https://gitlab.kitware.com/cmake/cmake/-/issues/25079
find_package(Protobuf CONFIG)
if(Protobuf_FOUND)
    set(PROTOBUF_TARGET protobuf::libprotobuf)
    get_target_property(PROTOBUF_INCLUDE_DIRS protobuf::libprotobuf INTERFACE_INCLUDE_DIRECTORIES)
else()
    # Fallback to `FindProtobuf` as shipped by CMake
    find_package(Protobuf)
    if(Protobuf_FOUND)
        if(Protobuf_VERSION VERSION_LESS 3.5)
            message(FATAL_ERROR "Version of Protobuf too old (${Protobuf_VERSION}), should be >=3.5.")
        elseif(NOT TARGET protobuf::libprotobuf)
            message(AUTHOR_WARNING "Cmake too old to define protobuf::libprotobuf, will try PkgConfig instead.")
        else()
            set(PROTOBUF_TARGET protobuf::libprotobuf)
            set(PROTOBUF_INCLUDE_DIRS ${Protobuf_INCLUDE_DIRS})
        endif()
    endif()
endif()

if(NOT DEFINED PROTOBUF_TARGET)
    find_dependency(PkgConfig)
    pkg_check_modules(protobuf REQUIRED IMPORTED_TARGET protobuf>=3.5)
    set(PROTOBUF_TARGET PkgConfig::protobuf)
    set(PROTOBUF_INCLUDE_DIRS ${protobuf_INCLUDE_DIRS})
endif()

find_package(gRPC)
if(gRPC_FOUND)
    set(GRPC_TARGET gRPC::grpc++)
else()
    find_dependency(PkgConfig)
    pkg_check_modules(grpc++ REQUIRED IMPORTED_TARGET grpc++>=1.10 grpc)
    set(GRPC_TARGET PkgConfig::grpc++)
endif()

if(NOT APPLE)
    find_dependency(PkgConfig)
    pkg_check_modules(uuid REQUIRED IMPORTED_TARGET uuid)
endif()


find_package(tomlplusplus)
if(NOT tomlplusplus_FOUND)
    find_dependency(PkgConfig)
    pkg_check_modules(tomlplusplus REQUIRED IMPORTED_TARGET tomlplusplus)
endif()

include("${CMAKE_CURRENT_LIST_DIR}/BuildboxGlogSetup.cmake")

#
# This file is being included from another package, so
# bring in Targets.cmake as well.
#
# If it is not, then Targets.cmake is not used (and not yet generated).
#
if(NOT "${BuildboxCommon_DIR}" STREQUAL "")
    include("${CMAKE_CURRENT_LIST_DIR}/BuildboxCommonTargets.cmake")
endif()
