What is buildbox-common?
========================

``buildbox-common`` is a library containing code used by multiple parts of
BuildBox and `recc <https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/recc/>`_. Its API is unstable
across major or minor versions and it should not be used by other applications.

Currently, ``buildbox-common`` contains all the Protocol Buffer definitions
used by BuildBox, as well as code to connect to and interact with
Content-Addressable Storage servers.

It also contains a metrics library ``buildboxcommonmetrics``, the details of
which can be found in it's own dedicated `README <buildbox-common/buildboxcommonmetrics/README.md>`_.

Tests for gRPC client code
--------------------------

The generated Protocol Buffer code includes mock client stubs, which can be used
to test gRPC client code. However, this is limited to sync gRPC client code.
Due to this, ``buildbox-common`` also includes a ``GrpcTestServer`` helper
class, which supports testing both, async and sync gRPC client code.

When migrating gRPC client code from sync to async, it's recommended to first
port the test cases from mock client stubs to ``GrpcTestServer`` such that the
updated test cases can be validated with the existing sync gRPC client code.

An example of ``GrpcTestServer`` usage can be found in
`buildboxcommon_remoteexecutionclient.t.cpp <test/buildboxcommon_remoteexecutionclient.t.cpp>`_.
