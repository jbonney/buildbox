/*
 * Copyright 2024 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCOMMON_LOGSTREAMREADER
#define INCLUDED_BUILDBOXCOMMON_LOGSTREAMREADER

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_grpcretrier.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_protos.h>

#include <iostream>
#include <string>

namespace buildboxcommon {

class LogStreamReader final {

  public:
    LogStreamReader(const std::string &resourceName,
                    const ConnectionOptions &connectionOptions);

    LogStreamReader(
        const std::string &resourceName,
        std::shared_ptr<google::bytestream::ByteStream::StubInterface>
            bytestreamClient,
        const int grpcRetryLimit, const int grpcRetryDelay);

    // Issue a `ByteStream.Read()` for the given resource name, and return
    // whether the read was successful.
    //
    // The provided `onMessage` callback function is called for each chunk
    // of data received from the `ByteStream.Read()` request, to allow the
    // caller to define behaviour for handling the data in the LogStream.
    bool read(std::function<void(const std::string &)> onMessage,
              std::function<bool()> shouldStop);

    // Default implementation of `read` which prints the LogStream contents
    // to stdout.
    bool read();

  private:
    const std::string d_resourceName;
    const unsigned int d_grpcRetryLimit;
    const std::chrono::milliseconds d_grpcRetryDelay;
    std::shared_ptr<google::bytestream::ByteStream::StubInterface>
        d_byteStreamClient;
};

} // namespace buildboxcommon

#endif