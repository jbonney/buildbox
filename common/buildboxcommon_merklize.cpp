// Copyright 2019 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <buildboxcommon_merklize.h>

#include <buildboxcommon_cashash.h>
#include <buildboxcommon_exception.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_stringutils.h>
#include <buildboxcommon_timeutils.h>

#include <cerrno>
#include <cstring>
#include <dirent.h>
#include <fnmatch.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <system_error>
#include <unistd.h>

namespace buildboxcommon {

namespace {

Digest hashFile(int fd) { return CASHash::hash(fd); }

void validateName(const std::string &name)
{
    if (name.empty() || name == "." || name == "..") {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::invalid_argument,
            "NestedDirectory: non-canonical path argument");
    }
}

} // namespace

File::File(const char *path,
           const std::vector<std::string> &capture_properties,
           const UnixModeUpdater &unixModeUpdater)
    : File(path, hashFile, capture_properties, unixModeUpdater)
{
}

File::File(const char *path, const FileDigestFunction &fileDigestFunc,
           const std::vector<std::string> &capture_properties,
           const UnixModeUpdater &unixModeUpdater)
    : File(AT_FDCWD, path, fileDigestFunc, capture_properties, unixModeUpdater)
{
}

File::File(int dirfd, const char *path,
           const FileDigestFunction &fileDigestFunc,
           const std::vector<std::string> &capture_properties,
           const UnixModeUpdater &unixModeUpdater)
{
    const FileDescriptor fd(openat(dirfd, path, O_RDONLY));
    if (fd.get() < 0) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category,
            "Failed to open path \"" << path << "\"");
    }
    init(fd.get(), fileDigestFunc, capture_properties, unixModeUpdater);
}

File::File(int fd, const std::vector<std::string> &capture_properties,
           const UnixModeUpdater &unixModeUpdater)
    : File(fd, hashFile, capture_properties, unixModeUpdater)
{
}

File::File(int fd, const FileDigestFunction &fileDigestFunc,
           const std::vector<std::string> &capture_properties,
           const UnixModeUpdater &unixModeUpdater)
{
    init(fd, fileDigestFunc, capture_properties, unixModeUpdater);
}

void File::init(int fd, const FileDigestFunction &fileDigestFunc,
                const std::vector<std::string> &capture_properties,
                const UnixModeUpdater &unixModeUpdater)
{
    const auto mode = FileUtils::getUnixMode(fd) & 07777;
    d_executable = (mode & S_IXUSR) != 0;
    d_digest = fileDigestFunc(fd);

    for (const std::string &property : capture_properties) {
        if (property == "mtime") {
            const auto mtime = FileUtils::getFileMtime(fd);
            d_nodeProperties.mutable_mtime()->CopyFrom(
                TimeUtils::make_timestamp(mtime));
        }
        else if (property == "unix_mode") {
            d_nodeProperties.mutable_unix_mode()->set_value(
                unixModeUpdater ? unixModeUpdater(mode) : mode);
        }
    }
}

FileNode File::to_filenode(const std::string &name) const
{
    FileNode result;
    result.set_name(name);
    *result.mutable_digest() = d_digest;
    result.set_is_executable(d_executable);

    if (d_nodeProperties.has_mtime() || d_nodeProperties.has_unix_mode() ||
        d_nodeProperties.properties_size() > 0) {
        result.mutable_node_properties()->CopyFrom(d_nodeProperties);
    }

    return result;
}

bool NestedDirectory::getSubdirAndNameForAdd(const char *relativePath,
                                             NestedDirectory **subdir,
                                             std::string *name)
{
    const char *slash = strrchr(relativePath, '/');
    if (slash) {
        *name = slash + 1;

        const std::string subdirKey(relativePath,
                                    static_cast<size_t>(slash - relativePath));
        if (subdirKey.empty()) {
            // Create entry in current directory
            *subdir = this;
        }
        else {
            *subdir = tryAddDirectory(subdirKey.c_str());
            if (*subdir == nullptr) {
                return false;
            }
        }
    }
    else {
        // Create entry in current directory
        *subdir = this;
        *name = relativePath;
    }

    validateName(*name);

    return true;
}

bool NestedDirectory::tryAddFile(const File &file, const char *relativePath)
{
    NestedDirectory *subdir;
    std::string name;

    if (!getSubdirAndNameForAdd(relativePath, &subdir, &name)) {
        return false;
    }

    if (subdir->d_files.count(name) > 0) {
        // Fail if existing file is different
        return file == subdir->d_files[name];
    }
    else if (subdir->d_symlinks.count(name) > 0 ||
             subdir->d_subdirs->count(name) > 0) {
        // Conflict with existing symlink or directory
        return false;
    }

    subdir->d_files[name] = file;
    return true;
}

void NestedDirectory::add(const File &file, const char *relativePath)
{
    if (!tryAddFile(file, relativePath)) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "NestedDirectory: Cannot add file, path already exists");
    }
}

bool NestedDirectory::tryAddSymlink(const std::string &target,
                                    const char *relativePath)
{
    NestedDirectory *subdir;
    std::string name;

    if (!getSubdirAndNameForAdd(relativePath, &subdir, &name)) {
        return false;
    }

    if (subdir->d_symlinks.count(name) > 0) {
        // Fail if existing symlink has a different target
        return target == subdir->d_symlinks[name];
    }
    else if (subdir->d_files.count(name) > 0 ||
             subdir->d_subdirs->count(name) > 0) {
        // Conflict with existing file or directory
        return false;
    }

    subdir->d_symlinks[name] = target;
    return true;
}

void NestedDirectory::addSymlink(const std::string &target,
                                 const char *relativePath)
{
    if (!tryAddSymlink(target, relativePath)) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "NestedDirectory: Cannot add symlink, path already exists");
    }
}

NestedDirectory *NestedDirectory::tryAddDirectory(const char *directory)
{
    // A forward slash by itself is not a valid input directory
    if (strcmp(directory, "/") == 0) {
        return this;
    }
    const char *slash = strchr(directory, '/');
    if (slash) {
        const std::string subdirKey(directory,
                                    static_cast<size_t>(slash - directory));
        if (subdirKey.empty()) {
            return this->tryAddDirectory(slash + 1);
        }
        else {
            validateName(subdirKey);
            if (d_files.count(subdirKey) > 0 ||
                d_symlinks.count(subdirKey) > 0) {
                // Conflict with existing file or symlink
                return nullptr;
            }
            return (*d_subdirs)[subdirKey].tryAddDirectory(slash + 1);
        }
    }
    else {
        const std::string name = directory;
        if ((*d_subdirs).count(name) == 0) {
            validateName(name);
            if (d_files.count(name) > 0 || d_symlinks.count(name) > 0) {
                // Conflict with existing file or symlink
                return nullptr;
            }
            (*d_subdirs)[name] = NestedDirectory();
        }
        return &(*d_subdirs)[name];
    }
}

void NestedDirectory::addDirectory(const char *directory)
{
    if (tryAddDirectory(directory) == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "NestedDirectory: Cannot create directory, path already exists");
    }
}

Digest NestedDirectory::to_digest(digest_string_map *digestMap) const
{
    // The 'd_files' and 'd_subdirs' maps make sure everything is sorted by
    // name thus the iterators will iterate lexicographically

    Directory directoryMessage;
    for (const auto &fileIter : d_files) {
        *directoryMessage.add_files() =
            fileIter.second.to_filenode(fileIter.first);
    }
    for (const auto &symlinkIter : d_symlinks) {
        SymlinkNode symlinkNode;
        symlinkNode.set_name(symlinkIter.first);
        symlinkNode.set_target(symlinkIter.second);
        *directoryMessage.add_symlinks() = symlinkNode;
    }
    for (const auto &subdirIter : *d_subdirs) {
        auto subdirNode = directoryMessage.add_directories();
        subdirNode->set_name(subdirIter.first);
        auto subdirDigest = subdirIter.second.to_digest(digestMap);
        *subdirNode->mutable_digest() = subdirDigest;
    }
    auto blob = directoryMessage.SerializeAsString();
    auto digest = make_digest(blob);
    if (digestMap != nullptr) {
        (*digestMap)[digest] = blob;
    }
    return digest;
}

Tree NestedDirectory::to_tree() const
{
    Tree result;
    auto root = result.mutable_root();
    for (const auto &fileIter : d_files) {
        *root->add_files() = fileIter.second.to_filenode(fileIter.first);
    }
    for (const auto &symlinkIter : d_symlinks) {
        SymlinkNode symlinkNode;
        symlinkNode.set_name(symlinkIter.first);
        symlinkNode.set_target(symlinkIter.second);
        *root->add_symlinks() = symlinkNode;
    }
    for (const auto &subdirIter : *d_subdirs) {
        auto subtree = subdirIter.second.to_tree();
        result.mutable_children()->MergeFrom(subtree.children());
        *result.add_children() = subtree.root();
        auto subdirNode = root->add_directories();
        subdirNode->set_name(subdirIter.first);
        *subdirNode->mutable_digest() = make_digest(subtree.root());
    }
    return result;
}

void NestedDirectory::print(std::ostream &out,
                            const std::string &dirName) const
{
    out << "directory: \"" << dirName << "\"" << std::endl;

    const std::string prefix = dirName.empty() ? "" : dirName + "/";

    out << d_files.size() << " files" << std::endl;
    for (const auto &it : d_files) {
        const std::string path = prefix + it.first;
        out << "    \"" << path << "\"" << std::endl;
    }

    out << d_symlinks.size() << " symlinks" << std::endl;
    for (const auto &it : d_symlinks) {
        const std::string path = prefix + it.first;
        out << "    \"" << path << "\", \"" << it.second << "\"" << std::endl;
    }

    out << d_subdirs->size() << " sub-directories" << std::endl << std::endl;
    for (const auto &it : *d_subdirs) {
        const std::string path = prefix + it.first;
        it.second.print(out, path);
    }
}

Digest make_digest(const std::string &blob) { return CASHash::hash(blob); }

static NestedDirectory
make_nesteddirectory(int basedirfd, const std::string prefix, const char *path,
                     const FileDigestFunction &fileDigestFunc,
                     digest_string_map *fileMap,
                     const std::vector<std::string> &capture_properties,
                     const bool followSymlinks,
                     const std::shared_ptr<IgnoreMatcher> ignoreMatcher,
                     const UnixModeUpdater &unixModeUpdater);

NestedDirectory
make_nesteddirectory(const char *path, digest_string_map *fileMap,
                     const bool followSymlinks,
                     const std::shared_ptr<IgnoreMatcher> ignoreMatcher,
                     const UnixModeUpdater &unixModeUpdater)
{
    const std::vector<std::string> capture_properties;
    return make_nesteddirectory(path, fileMap, capture_properties,
                                followSymlinks, ignoreMatcher,
                                unixModeUpdater);
}

NestedDirectory
make_nesteddirectory(const char *path, digest_string_map *fileMap,
                     const std::vector<std::string> &capture_properties,
                     const bool followSymlinks,
                     const std::shared_ptr<IgnoreMatcher> ignoreMatcher,
                     const UnixModeUpdater &unixModeUpdater)
{
    return make_nesteddirectory(path, hashFile, fileMap, capture_properties,
                                followSymlinks, ignoreMatcher,
                                unixModeUpdater);
}

NestedDirectory
make_nesteddirectory(const char *path,
                     const FileDigestFunction &fileDigestFunc,
                     digest_string_map *fileMap,
                     const std::vector<std::string> &capture_properties,
                     const bool followSymlinks,
                     const std::shared_ptr<IgnoreMatcher> ignoreMatcher,
                     const UnixModeUpdater &unixModeUpdater)
{
    return make_nesteddirectory(AT_FDCWD, "", path, fileDigestFunc, fileMap,
                                capture_properties, followSymlinks,
                                ignoreMatcher, unixModeUpdater);
}

NestedDirectory
make_nesteddirectory(int dirfd, digest_string_map *fileMap,
                     const std::vector<std::string> &capture_properties,
                     const bool followSymlinks,
                     const std::shared_ptr<IgnoreMatcher> ignoreMatcher,
                     const UnixModeUpdater &unixModeUpdater)
{
    return make_nesteddirectory(dirfd, "", "", hashFile, fileMap,
                                capture_properties, followSymlinks,
                                ignoreMatcher, unixModeUpdater);
}

NestedDirectory
make_nesteddirectory(int dirfd, const FileDigestFunction &fileDigestFunc,
                     digest_string_map *fileMap,
                     const std::vector<std::string> &capture_properties,
                     const bool followSymlinks,
                     const std::shared_ptr<IgnoreMatcher> ignoreMatcher,
                     const UnixModeUpdater &unixModeUpdater)
{
    return make_nesteddirectory(dirfd, "", "", fileDigestFunc, fileMap,
                                capture_properties, followSymlinks,
                                ignoreMatcher, unixModeUpdater);
}

NestedDirectory
make_nesteddirectory(int basedirfd, const std::string prefix, const char *path,
                     const FileDigestFunction &fileDigestFunc,
                     digest_string_map *fileMap,
                     const std::vector<std::string> &capture_properties,
                     const bool followSymlinks,
                     const std::shared_ptr<IgnoreMatcher> ignoreMatcher,
                     const UnixModeUpdater &unixModeUpdater)
{
    NestedDirectory result;
    int dirfd;
    const bool empty_path = strlen(path) == 0;
    if (empty_path) {
        dirfd = dup(basedirfd);
        if (dirfd < 0) {
            BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
                std::system_error, errno, std::system_category,
                "Failed to dup() directory file descriptor");
        }
    }
    else {
        dirfd = openat(basedirfd, path, O_RDONLY | O_DIRECTORY);
        if (dirfd < 0) {
            BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
                std::system_error, errno, std::system_category,
                "Failed to open path \"" << path << "\"");
        }
    }
    const auto dir = fdopendir(dirfd);
    if (dir == nullptr) {
        close(dirfd);
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category,
            "Failed to open path \"" << path << "\"");
    }

    const std::string newprefix(empty_path ? prefix : (prefix + path + "/"));
    for (auto dirent = readdir(dir); dirent != nullptr;
         dirent = readdir(dir)) {
        if (strcmp(dirent->d_name, ".") == 0 ||
            strcmp(dirent->d_name, "..") == 0) {
            continue;
        }

        const std::string entityName(dirent->d_name);
        const std::string entityPath = newprefix + entityName;
        if (ignoreMatcher != nullptr && ignoreMatcher->match(entityPath)) {
            continue;
        }

        struct stat statResult;
        int statFlags = 0;
        if (!followSymlinks) {
            statFlags = AT_SYMLINK_NOFOLLOW;
        }
        if (fstatat(dirfd, dirent->d_name, &statResult, statFlags) != 0) {
            continue;
        }

        if (S_ISDIR(statResult.st_mode)) {
            if (ignoreMatcher != nullptr &&
                ignoreMatcher->match(entityPath + "/")) {
                continue;
            }
            (*result.d_subdirs)[entityName] = make_nesteddirectory(
                dirfd, newprefix, dirent->d_name, fileDigestFunc, fileMap,
                capture_properties, followSymlinks, ignoreMatcher,
                unixModeUpdater);
        }
        else if (S_ISREG(statResult.st_mode)) {
            const File file(dirfd, dirent->d_name, fileDigestFunc,
                            capture_properties, unixModeUpdater);
            result.d_files[entityName] = file;

            if (fileMap != nullptr) {
                (*fileMap)[file.d_digest] = entityPath;
            }
        }
        else if (S_ISLNK(statResult.st_mode)) {
            std::string target(static_cast<size_t>(statResult.st_size), '\0');

            if (readlinkat(dirfd, dirent->d_name, &target[0], target.size()) <
                0) {
                closedir(dir);
                BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
                    std::system_error, errno, std::system_category,
                    "Error reading symlink at \""
                        << entityPath
                        << "\", st_size = " << statResult.st_size);
            }
            result.d_symlinks[entityName] = target;
        }
    }

    // This will implicitly close `dirfd`.
    closedir(dir);
    return result;
}

std::ostream &operator<<(std::ostream &out, const NestedDirectory &obj)
{
    obj.print(out);
    return out;
}

bool IgnorePattern::operator==(const IgnorePattern &other) const
{
    return d_pattern == other.d_pattern &&
           d_matchBasenameOnly == other.d_matchBasenameOnly;
}

IgnoreMatcher::IgnoreMatcher(
    const std::string &pathPrefix,
    const std::shared_ptr<std::vector<IgnorePattern>> &ignorePatterns)
    : d_pathPrefix(pathPrefix), d_ignorePatterns(ignorePatterns)
{
}

bool IgnoreMatcher::match(const std::string &path,
                          const int fnmatchFlags) const
{
    const auto relativePath = trimPrefix(path);
    for (const auto &pattern : *d_ignorePatterns) {
        bool matched = false;
        if (pattern.d_matchBasenameOnly) {
            std::string filename =
                FileUtils::pathBasename(relativePath.c_str());
            // preserve the last slash given it's trimmed in `pathBasename`
            if (relativePath.back() == '/') {
                filename += '/';
            }
            matched = fnmatch(pattern.d_pattern.c_str(), filename.c_str(),
                              fnmatchFlags) == 0;
        }
        else {
            matched = fnmatch(pattern.d_pattern.c_str(), relativePath.c_str(),
                              fnmatchFlags | FNM_PATHNAME) == 0;
        }

        if (matched) {
            return true;
        }
    }
    return false;
}

std::string IgnoreMatcher::trimPrefix(const std::string &path) const
{
    if (d_pathPrefix.empty() || path.find(d_pathPrefix) == std::string::npos) {
        return path;
    }
    size_t startIdx = d_pathPrefix.size();
    // skip through beginning slashes
    while (startIdx != path.size() && path[startIdx] == '/') {
        startIdx++;
    }
    return path.substr(startIdx);
}

std::shared_ptr<std::vector<IgnorePattern>>
IgnoreMatcher::parseIgnorePatterns(std::istream &is)
{
    std::string line;
    auto patterns = std::make_shared<std::vector<IgnorePattern>>();
    while (std::getline(is, line)) {
        const auto slashIdx = line.find('/');
        // If slash only occurs at the end
        const bool matchBasenameOnly =
            slashIdx == std::string::npos || slashIdx == line.size() - 1;
        // remove trailing spaces and beginning slashes
        StringUtils::rtrim(&line);
        StringUtils::ltrim(&line, [](char c) { return c == '/'; });

        if (!line.empty()) {
            patterns->emplace_back(line, matchBasenameOnly);
        }
    }

    return patterns;
}

} // namespace buildboxcommon
