#ifndef INCLUDED_BUILDBOXCOMMON_PLATFORMUTILS
#define INCLUDED_BUILDBOXCOMMON_PLATFORMUTILS

#include <string>
#include <unordered_set>

namespace buildboxcommon {

struct PlatformUtils {

    /**
     * Special actions which are required to support this ISA
     */
    enum ISASupportAction {
        ISA_SUPPORT_ACTION_NONE,        // No special action required
        ISA_SUPPORT_ACTION_RUN_LINUX32, // Use the linux32 program
    };

    /**
     * Reports if and how a given ISA is supported on this host
     */
    struct ISASupport {
        bool supported;          // Whether this ISA is supported
        ISASupportAction action; // Special support actions
    };

    /**
     * Return REAPI OSFamily of running system.
     */
    static std::string getHostOSFamily();

    /**
     * Return REAPI ISA of running system.
     */
    static std::string getHostISA();

    /**
     * Return a set of REAPI ISAs which can be supported on this host
     */
    static std::unordered_set<std::string> getSupportedISAs();

    /**
     * Returns an ISASupport describing whether the requested ISA is supported
     */
    static ISASupport getISASupport(const std::string &isa);
};

} // namespace buildboxcommon

#endif // INCLUDED_BUILDBOXCOMMON_PLATFORMUTILS
