/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_systemutils.h>

#include <buildboxcommon_exception.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>

#include <array>
#include <cerrno>
#include <cstring>
#include <memory>
#include <sstream>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

using namespace buildboxcommon;

namespace {
std::string logLineForCommand(const std::vector<std::string> &command)
{
    std::ostringstream command_string;
    for (const auto &token : command) {
        command_string << token << " ";
    }
    return command_string.str();
}

static std::array<int, 2> createPipe()
{
    std::array<int, 2> pipe_fds = {0, 0};

    if (pipe(pipe_fds.data()) == -1) {
        BUILDBOX_LOG_ERROR("Error calling `pipe()`: " << strerror(errno));
        throw std::system_error(errno, std::system_category());
    }
    return pipe_fds;
}
} // namespace

int SystemUtils::executeCommand(const std::vector<std::string> &command)
{
    const auto argc = command.size();

    // `execvp()` takes a `const char*[]`, making the conversion:
    const auto argv = std::make_unique<const char *[]>(argc + 1);
    for (unsigned int i = 0; i < argc; ++i) {
        argv[i] = command[i].c_str();
    }
    argv[argc] = nullptr;

    execv(argv[0], const_cast<char *const *>(argv.get()));
    // `execv()` does NOT search for binaries using $PATH.
    // ---------------------------------------------------------------------
    // The lines below will only be executed if `execv()` failed, otherwise
    // `execv()` does not return.

    const auto exec_error = errno;
    const auto exec_error_reason = strerror(errno);

    BUILDBOX_LOG_ERROR("Error while calling `execv("
                       << logLineForCommand(command)
                       << ")`: " << exec_error_reason);

    // Following the Bash convention for exit codes.
    // (https://gnu.org/software/bash/manual/html_node/Exit-Status.html)
    if (exec_error == ENOENT) {
        return 127; // "command not found"
    }
    else {
        return 126; // Command invoked cannot execute
    }
}

int SystemUtils::executeCommandAndWait(const std::vector<std::string> &command)
{
    const pid_t pid = fork();
    if (pid == -1) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(std::system_error, errno,
                                              std::system_category,
                                              "Error calling fork()");
    }

    if (pid == 0) { // Child process
        const auto status = executeCommand(command);

        // Only reached if the underlying call to exec() failed.
        // `executeCommand()` will log that.
        _Exit(status);
    }
    else { // Parent process
        return waitPid(pid);
    }
}

SystemUtils::ExecutionResult
SystemUtils::executeCommandWithResult(const std::vector<std::string> &command,
                                      bool pipeStdOut, bool pipeStdErr)
{
    // Convert the command to a char*[]
    size_t argc = command.size();
    std::unique_ptr<const char *[]> argv(new const char *[argc + 1]);
    for (size_t i = 0; i < argc; ++i) {
        argv[i] = command[i].c_str();
    }
    argv[argc] = nullptr;

    // Pipe, fork and exec
    auto stdOutPipeFDs = createPipe();
    auto stdErrPipeFDs = createPipe();

    const auto pid = fork();
    if (pid == -1) {
        BUILDBOX_LOG_ERROR("Error calling `fork()`: " << strerror(errno));
        throw std::system_error(errno, std::system_category());
    }

    if (pid == 0) {
        // (runs only in the child)
        if (pipeStdOut) {
            close(stdOutPipeFDs[0]);
            dup2(stdOutPipeFDs[1],
                 STDOUT_FILENO); // redirect stdout to input end of pipe
            close(stdOutPipeFDs[1]);
        }
        if (pipeStdErr) {
            close(stdErrPipeFDs[0]);
            dup2(stdErrPipeFDs[1],
                 STDERR_FILENO); // redirect stderr to input end of pipe
            close(stdErrPipeFDs[1]);
        }

        const auto exec_status =
            execvp(argv[0], const_cast<char *const *>(argv.get()));

        int exit_code = 1;
        if (exec_status != 0) {
            const auto exec_error = errno;
            // Following the Bash convention for exit codes.
            // (https://gnu.org/software/bash/manual/html_node/Exit-Status.html)
            if (exec_error == ENOENT) {
                exit_code = 127; // "command not found"
            }
            else {
                exit_code = 126; // Command invoked cannot execute
            }
        }

        _Exit(exit_code);
    }

    // (runs only in the parent)
    ExecutionResult result;

    // Get the output from the child process
    fd_set fdSet;
    FD_ZERO(&fdSet);
    if (pipeStdOut) {
        FD_SET(stdOutPipeFDs[0], &fdSet);
        close(stdOutPipeFDs[1]);
    }
    if (pipeStdErr) {
        FD_SET(stdErrPipeFDs[0], &fdSet);
        close(stdErrPipeFDs[1]);
    }

    char buffer[4096];
    while (FD_ISSET(stdOutPipeFDs[0], &fdSet) ||
           FD_ISSET(stdErrPipeFDs[0], &fdSet)) {
        fd_set readFDSet = fdSet;

        struct timeval timeout;
        timeout.tv_sec = 5;
        timeout.tv_usec = 0;

        select(FD_SETSIZE, &readFDSet, nullptr, nullptr, &timeout);

        if (FD_ISSET(stdOutPipeFDs[0], &readFDSet)) {
            ssize_t bytesRead = read(stdOutPipeFDs[0], buffer, sizeof(buffer));
            if (bytesRead > 0) {
                result.d_stdOut.append(buffer, static_cast<size_t>(bytesRead));
            }
            else {
                close(stdOutPipeFDs[0]);
                FD_CLR(stdOutPipeFDs[0], &fdSet);
            }
        }
        if (FD_ISSET(stdErrPipeFDs[0], &readFDSet)) {
            ssize_t bytesRead = read(stdErrPipeFDs[0], buffer, sizeof(buffer));
            if (bytesRead > 0) {
                result.d_stdErr.append(buffer, static_cast<size_t>(bytesRead));
            }
            else {
                close(stdErrPipeFDs[0]);
                FD_CLR(stdErrPipeFDs[0], &fdSet);
            }
        }
    }

    // Get the status code from the child process
    result.d_exitCode = waitPidOrSignal(pid);

    return result;
}

std::string SystemUtils::getPathToCommand(const std::string &command)
{
    if (command.find('/') != std::string::npos) {
        return command; // `command` is a path, no need to search.
    }

    // Reading $PATH, parsing it, and looking for the binary:
    char *path_pointer = getenv("PATH");
    if (path_pointer == nullptr) {
        BUILDBOX_LOG_ERROR("Could not read $PATH");
        return "";
    }

    std::istringstream iss(path_pointer);

    char separator = ':';
    for (std::string token; std::getline(iss, token, separator);) {
        const std::string path = token + "/" + command;

        if (FileUtils::isRegularFile(path.c_str()) &&
            FileUtils::isExecutable(path.c_str())) {
            return path;
        }
    }

    return "";
}

int SystemUtils::waitPid(const pid_t pid)
{
    while (true) {
        const int exit_code = waitPidOrSignal(pid);
        if (exit_code == -EINTR) {
            // The child can still run, keep waiting for it.
            continue;
        }
        else {
            return exit_code;
        }
    }
}

int SystemUtils::waitPidOrSignal(const pid_t pid)
{
    int status;
    const pid_t child_pid = waitpid(pid, &status, 0);

    if (child_pid == -1) { // `waitpid()` failed
        const auto waitpid_error = errno;

        if (waitpid_error == EINTR) {
            // Signal caught before child terminated.
            return -EINTR;
        }

        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(std::system_error, errno,
                                              std::system_category,
                                              "Error in waitpid()");
    }

    if (WIFEXITED(status)) {
        return WEXITSTATUS(status);
    }

    if (WIFSIGNALED(status)) {
        return 128 + WTERMSIG(status);
        // Exit code as returned by Bash.
        // (https://gnu.org/software/bash/manual/html_node/Exit-Status.html)
    }

    /* According to the documentation for `waitpid(2)` we should never get
     * here:
     *
     * "If the information pointed to by stat_loc was stored by a call to
     * waitpid() that did not specify the WUNTRACED  or
     * CONTINUED flags, or by a call to the wait() function,
     * exactly one of the macros WIFEXITED(*stat_loc) and
     * WIFSIGNALED(*stat_loc) shall evaluate to a non-zero value."
     *
     * (https://pubs.opengroup.org/onlinepubs/009695399/functions/wait.html)
     */
    BUILDBOXCOMMON_THROW_EXCEPTION(
        std::runtime_error,
        "`waitpid()` returned an unexpected status: " << status);
}

std::string SystemUtils::getCurrentWorkingDirectory()
{
    unsigned int bufferSize = 1024;
    while (true) {
        std::unique_ptr<char[]> buffer(new char[bufferSize]);
        char *cwd = getcwd(buffer.get(), bufferSize);

        if (cwd != nullptr) {
            return std::string(cwd);
        }
        else if (errno == ERANGE) {
            bufferSize *= 2;
        }
        else {
            BUILDBOXCOMMON_THROW_EXCEPTION(
                std::runtime_error, "current working directory not found");
        }
    }
}

void SystemUtils::redirectStandardOutputToFile(const int standardOutputFd,
                                               const std::string &path)
{
    if (standardOutputFd != STDOUT_FILENO &&
        standardOutputFd != STDERR_FILENO) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::invalid_argument,
            "File descriptor it not `STDOUT_FILENO` or `STDERR_FILENO`.");
    }

    const FileDescriptor fileFd(open(path.c_str(),
                                     O_CREAT | O_TRUNC | O_APPEND | O_WRONLY,
                                     S_IRUSR | S_IWUSR));

    if (fileFd.get() == -1 || dup2(fileFd.get(), standardOutputFd) == -1) {
        const auto outputName =
            (standardOutputFd == STDOUT_FILENO) ? "stdout" : "stderr";

        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category,
            "Error redirecting " << outputName << " to \"" << path << "\"");
    }
}

ProcessCredentials SystemUtils::getProcessCredentials()
{
    ProcessCredentials creds;
    creds.uid = geteuid();
    creds.gid = getegid();
    return creds;
}

std::unordered_set<gid_t> SystemUtils::getGids()
{
    std::unordered_set<gid_t> gids;

    // effective gid
    // It is unspecified whether the effective group ID of the calling process
    // of `getgroups` is included in the returned list.
    gids.emplace(getegid());

    // supplement gids
    // get total size
    int count = 0;
    if ((count = getgroups(0, nullptr)) == -1) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error, "Failed to get the size of group access list "
                                "for current process, error:"
                                    << std::strerror(errno));
    }
    std::vector<gid_t> gidBuffer(static_cast<size_t>(count));

    if (getgroups(count, gidBuffer.data()) == -1) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "Failed to get the group access list for current process, error:"
                << std::strerror(errno));
    }

    gids.insert(gidBuffer.begin(), gidBuffer.end());
    return gids;
}
