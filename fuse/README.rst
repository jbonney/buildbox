What is buildbox-fuse?
=================

buildbox-fuse is a tool to run build commands in a sandbox with CAS as storage
backend, as defined by the Bazel `Remote Execution API`_.  FUSE is used to
provide POSIX filesystem access to directories and files stored in CAS.

It is designed to work with `BuildStream`_ but does not depend on it.

.. _Remote Execution API: https://github.com/bazelbuild/remote-apis
.. _BuildStream: https://wiki.gnome.org/Projects/BuildStream


Usage
=====

  buildbox-fuse [OPTIONS] MOUNTPOINT

This mounts a CAS directory at the specified mount point using fuse.

Options:

* --local=PATH (required)

  Path to local CAS cache directory. The filesystem structure matches the
  BuildStream CAS. An empty directory can be specified if no existing cache
  is to be reused.

* --input-digest=PATH

  Path to a file containing a REAPI digest in protobuf format pointing to
  the initial state of the sandbox (input directory). Without this option,
  the sandbox starts with an empty directory.
  Cannot be used with `input-digest-value`.

* --input-digest-value=<hash>/<size>

  Specify the input digest to be used for the initial state of the sandbox on
  the command line as the hash and size separated by forward-slash delimiter.
  Cannot be used with `input-digest`.

* --output-digest=PATH (required)

  Path where to store the REAPI digest in protobuf format pointing to the
  final state of the sandbox (output directory). This file is written at
  process termination.

* --chdir=PATH

  Set working directory to the specified path instead of the sandbox root
  directory.

* --remote=URL

  The URL for the remote CAS server. Objects that are not in the local CAS
  cache will be downloaded from the specified remote CAS server as needed.
  Modified files and directories will be uploaded upon buildbox termination.
  If this option is not specified, all objects must be available in the
  local CAS cache.

* --server-cert=PATH

  Path to the PEM-encoded public server certificate for https connections to
  the remote CAS server. This allows use of self-signed certificates.

* --client-key=PATH

  Path to the PEM-encoded private client key for https with
  certificate-based client authentication. If this is specified,
  ``--client-cert`` must be specified as well.

* --client-cert=PATH

  Path to the PEM-encoded public client certificate for https with
  certificate-based client authentication. If this is specified,
  ``--client-key`` must be specified as well.

* --prefetch

  Download all files from the remote CAS server on startup to reduce access
  latency.

* --output-times=PATH

  Specify a file location to output timestamps. This is written as a serialized
  protocol buffer containing a ``ExecutedActionMetadata`` message.
