#!/bin/bash

if [ -z "$CAS_SERVER" ] ; then
	echo "Missing buildbox-casd path" >&2
	exit 1
fi

remote_cleanup() {
	stop_cas_server
	cleanup
}
trap remote_cleanup INT TERM EXIT

start_cas_server() {
	if ! [ -x "$(command -v netstat)" ]; then
		echo "netstat is required for running network tests" >&2
		exit 1
	fi
	"$CAS_SERVER" --bind localhost:0 "$TMPDIR/cas-remote" &
	CAS_SERVER_PID=$!
	PORT=
	while [ -z $PORT ] ; do
		sleep 0.1
		[ -e /proc/$CAS_SERVER_PID ]
		PORT=$(netstat -lntp 2>/dev/null | grep "LISTEN.* $CAS_SERVER_PID" | head -n 1 | sed -e 's/.*:\([0-9]\+\).*/\1/')
	done
	REMOTE_ARGS="--remote=http://localhost:$PORT"
	if [ -n "$PREFETCH" ] ; then
		REMOTE_ARGS="$REMOTE_ARGS --prefetch"
	fi
}

stop_cas_server() {
	if [ -n "$CAS_SERVER_PID" ] ; then
		kill $CAS_SERVER_PID

		# Wait for buildbox-casd to terminate
		while [ -e "/proc/$CAS_SERVER_PID" ] ; do
			sleep 0.1
		done

		CAS_SERVER_PID=
	fi
}
