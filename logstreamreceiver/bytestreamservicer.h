/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_protos.h>

#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_context.h>

#include <functional>
#include <string>

#ifndef INCLUDED_BYTESTREAM_SERVICER
#define INCLUDED_BYTESTREAM_SERVICER

class LogStreamBytestreamServicer final
    : public google::bytestream::ByteStream::Service {
  public:
    typedef std::function<void(const std::string &resourceName,
                               const std::string &data,
                               const bool writeFinished)>
        DataReceivedCallback;

    explicit LogStreamBytestreamServicer(const DataReceivedCallback &callback);
    ~LogStreamBytestreamServicer();

    grpc::Status Read(
        grpc::ServerContext *, const google::bytestream::ReadRequest *request,
        grpc::ServerWriter<google::bytestream::ReadResponse> *writer) override;

    grpc::Status
    Write(grpc::ServerContext *,
          grpc::ServerReader<google::bytestream::WriteRequest> *request,
          google::bytestream::WriteResponse *response) override;

    grpc::Status QueryWriteStatus(
        grpc::ServerContext *,
        const google::bytestream::QueryWriteStatusRequest *request,
        google::bytestream::QueryWriteStatusResponse *response) override;

  private:
    const DataReceivedCallback d_dataReceivedCallback;

    static std::string
    printableWriteRequest(const google::bytestream::WriteRequest &request);
};

#endif
