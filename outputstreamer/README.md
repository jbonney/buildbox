# outputstreamer

This program reads from `stdin` and streams it to the given ByteStream endpoint.

## Usage

```
Usage: ./outputstreamer
   --help                      Display usage and exit
   --resource-name             LogStream write resource name. If not provided, creates one calling CreateLogStream() [optional]
   --resource-parent           The parent resource name given to CreateLogStream() [optional, default = ""]
   --file                      File to stream (if not specified, stdin) [optional]
   --follow                    Read file and stream changes until signaled to exit. (Only with --file.) [optional]
   --log-level                 Log level (debug, error, info, trace, warning) [optional, default = "error"]
   --verbose                   Set log level to 'debug' [optional]
   --remote                    URL for all services [optional]
   --instance                  Instance for all services [optional]
   --server-cert               Server TLS certificate for all services (PEM-encoded) [optional]
   --client-key                Client private TLS key far all services (PEM-encoded) [optional]
   --client-cert               Client TLS certificate for all services (PEM-encoded) [optional]
   --access-token              Authentication token for all services (JWT, OAuth token etc), will be included as an HTTP Authorization bearer token [optional]
   --token-reload-interval     Default access token refresh timeout [optional]
   --googleapi-auth            Use GoogleAPIAuth for all services [optional]
   --retry-limit               Retry limit for gRPC errors for all services [optional]
   --retry-delay               Retry delay for gRPC errors for all services [optional]
   --request-timeout           Timeout for gRPC requests for all services  (set to 0 to disable timeout) [optional]
   --min-throughput            Minimum throughput for gRPC requests for all services, bytes per seconds.
                                  The value may be suffixed with K, M, G or T. [optional]
   --keepalive-time            gRPC keepalive pings period for all services (set to 0 to disable keepalive pings) [optional]
   --load-balancing-policy     gRPC load balancing policy for all services (valid options are 'round_robin' and 'grpclb') [optional]
```

## Example usage

```bash
cat /proc/cpuinfo | ./outputstreamer --remote="http://localhost:50070" --resource-name="cpuinfo-output-log"
```

It can also be used interactively, hitting `Ctrl+D` (`^D`) to signal the end of file.

```bash
./outputstreamer --remote="http://localhost:50070" --resource-name="user-input-log"
```

The `logstreamreceiver` tool in this repo can be used to receive those outputs on the other end.
