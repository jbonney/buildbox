// Copyright (C) 2022 Bloomberg LP
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


syntax = "proto3";

package trexe;

import "google/rpc/status.proto";
import "build/bazel/remote/execution/v2/remote_execution.proto";

// Metadata of the ActionResult
message ActionResultMetadata {
    int32 exit_code = 1;

    // True if the result was served from cache, false if it was executed.
    bool  cached_result = 2;
}

// Metadata of the Operation
message OperationMetadata {
    // Name of the Operation
    string name = 1;

    // Error status of the Operation if any
    google.rpc.Status operation_error = 2;
}

// Result metadata of the trexe command
message TrexeResultMetadata {
    // Error status of Operation if any
    OperationMetadata operation_metadata = 1;

    // Metadata of the downloaded ActionResult
    ActionResultMetadata action_result_metadata = 2;

    // Error of trexe or BuildGrid
    google.rpc.Status error = 3;

    // Digest of underlying `Action`
    // Note: It is not a part of `ActionResultMetadata` as the digest can exist
    // before the operation is created or its result is returned.
    build.bazel.remote.execution.v2.Digest action_digest = 4;
}
