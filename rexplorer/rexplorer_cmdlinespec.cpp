/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <rexplorer_cmdlinespec.h>

#include <buildboxcommon_connectionoptions_commandline.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_logging_commandline.h>

namespace rexplorer {

using ArgumentSpec = buildboxcommon::CommandLineTypes::ArgumentSpec;
using DataType = buildboxcommon::CommandLineTypes::DataType;
using TypeInfo = buildboxcommon::CommandLineTypes::TypeInfo;
using DefaultValue = buildboxcommon::CommandLineTypes::DefaultValue;

CmdLineSpec::CmdLineSpec()
{
    d_spec.emplace_back(
        "action", "Action digest to view. Must be of the form <hash>/<size>",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_REQUIRED,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "depth",
        "How many levels of the input root to fetch, a negative "
        "value means print the entire root",
        TypeInfo(DataType::COMMANDLINE_DT_INT), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG, DefaultValue(1));

    auto loggingSpec = buildboxcommon::loggingCommandLineSpec();

    d_spec.insert(d_spec.end(), loggingSpec.cbegin(), loggingSpec.cend());

    d_spec.emplace_back("pretty", "Print human readable JSON",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL));

    // Default values for connections
    const auto connectionOptionsSpec =
        buildboxcommon::ConnectionOptionsCommandLine("", "");
    d_spec.insert(d_spec.end(), connectionOptionsSpec.spec().cbegin(),
                  connectionOptionsSpec.spec().cend());

    // CAS connection:
    const auto casConnectionOptionsSpec =
        buildboxcommon::ConnectionOptionsCommandLine("CAS", "cas-");
    d_spec.insert(d_spec.end(), casConnectionOptionsSpec.spec().cbegin(),
                  casConnectionOptionsSpec.spec().cend());

    // AC connection:
    const auto acConnectionOptionsSpec =
        buildboxcommon::ConnectionOptionsCommandLine("Action Cache", "ac-");
    d_spec.insert(d_spec.end(), acConnectionOptionsSpec.spec().cbegin(),
                  acConnectionOptionsSpec.spec().cend());
};

} // namespace rexplorer
