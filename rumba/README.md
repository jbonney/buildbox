# rumba

## About

`rumba` is a compiler wrapper designed to collect and publish higher-level compilation metadata.
It is intended for comparative performance testing of compilers and compiler wrappers. `rumba` is designed
to work with [`recc`](https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/recc/), but it is generically compatible with any compiler
or compiler wrapper. It publishes metadata pertaining to compilation commands to a configurable UDP port on localhost.

`rumbad` is a daemon that can listen for on the above socket for the protobuf, then publish the data to a persistent storage backend. Currently, `rumbad` only supports writing the data to standard output.

## Usage

### rumba
Simply wrap your compile command with `rumba`, e.g.

```
rumba /usr/bin/gcc -c hello.cpp -o hello.o
```

You can also specify config using a file. The file 
```
rumba -c <config-file-directory> <command>
```
The config-file-directory should contain a rumba.conf for it to affect the configuration. If the directory also contains a recc.conf it overrides the RECC_CONFIG_DIRECTORY environment variable

### rumbad
```
rumbad <PORT>
```

The port is a UDP port on localhost.

## Configuration

### rumba
`rumba` reads the following environment variables as configurable parameters:
- `RUMBA_PORT`: A port on localhost to write to. Defaults to `19111`.
- `RUMBA_CORRELATED_INVOCATIONS_ID`: An id to link multiple invocations of rumba together. If not specified, a random UUID will be generated for the invocation.
- `RUMBA_LOG_LEVEL` - Logging verbosity level (optional, default = error, supported = trace/debug/info/warning/error)
- `RUMBA_VERBOSE` - If set to any value, equivalent to `RUMBA_LOG_LEVEL=debug`
- `RUMBA_LOG_DIRECTORY` - Instead of printing to stderr, write log files in this location (follows [glog's file-naming convention](https://github.com/google/glog#severity-levels))
- `RUMBA_USE_RECC` - If set to any value, use recc to invoke the command to support caching and optional remote execution. Configure recc with [`RECC_*` environment variables](https://buildgrid.gitlab.io/recc/configuration-variables.html) and/or `recc.conf`.
- `RUMBA_VERIFY` - If set to any value, invoke the command both locally and remotely for verification purposes. Output digests are compared and logged. This requires `RUMBA_USE_RECC` to be set and recc configured for remote execution.

### rumbad
`rumbad` reads the following environment variables as configurable parameters:
- `RUMBAD_RECV_BUFFER_SIZE`: Sets the value of `SO_RCVBUF` on the socket `rumbad` listens to.
- `RUMBAD_PUBLISH_INTERVAL`: The number of seconds to wait between writing batches of messages to the persistent storage.
