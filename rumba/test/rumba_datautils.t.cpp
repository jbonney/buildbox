// Copyright 2021 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// component under test
#include <build/buildbox/local_execution.pb.h>
#include <rumba_datautils.h>

#include <google/protobuf/text_format.h>
#include <google/protobuf/util/message_differencer.h>

#include <netinet/in.h>
#include <sys/resource.h>
#include <sys/socket.h>

#include <buildboxcommon_platformutils.h>
#include <buildboxcommon_systemutils.h>

// third party includes
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace rumba {
namespace test {

using namespace ::testing;

namespace {
void buildCompilationData(
    build::buildbox::CompilerExecutionData &compilationData)
{
    std::string cwd =
        buildboxcommon::SystemUtils::getCurrentWorkingDirectory();

    // Setting the last param to 0 prevents overwrite if exists
    setenv("FAVORITE_FOOD", "PIZZA", 0);
    setenv("EMPTY_VARIABLE", "", 0);

    const char *argv[] = {"rumba", "gcc",    "-c", "data/hello/hello.cpp",
                          "-o",    "hello.o"};
    const size_t argc = (sizeof(argv) / sizeof(const char *));

    const int cmd_argc = argc - 1;
    const char **cmd_argv = &argv[1];

    DataUtils::collectCompilationData(cmd_argc, cmd_argv, "gcc",
                                      compilationData);
}

void clearEnv()
{

    // Set globals to default
    RUMBA_LOG_DIRECTORY = "";
    RUMBA_VERBOSE = false;
    RUMBA_LOG_LEVEL = "error";
    RUMBA_USE_RECC = false;
    RUMBA_VERIFY = false;
    RUMBA_PORT = "19111";
    RUMBA_CORRELATED_INVOCATIONS_ID = "";

    // Clear environment variables
    unsetenv("RUMBA_LOG_DIRECTORY");
    unsetenv("RUMBA_VERBOSE");
    unsetenv("RUMBA_LOG_LEVEL");
    unsetenv("RUMBA_USE_RECC");
    unsetenv("RUMBA_VERIFY");
    unsetenv("RUMBA_PORT");
    unsetenv("RUMBA_CORRELATED_INVOCATIONS_ID");
}

} // namespace

TEST(CollectCompilationData, AllExtensionsAsExpected)
{
    clearEnv();
    std::string cwd =
        buildboxcommon::SystemUtils::getCurrentWorkingDirectory();
    std::string absoluteFilePath = cwd + "/data/hello/hello.cpp";
    std::vector<const char *> sourceFiles = {"data/hello/hello.cpp",
                                             "data/hello/hello.m.cpp",
                                             "data/hello/hello.cc",
                                             "data/hello/hello.c++",
                                             "data/hello/hello.cp",
                                             "data/hello/hello.cxx",
                                             "data/hello/hello.c",
                                             "data/hello/uppercase/hello.C",
                                             "data/hello/uppercase/hello.CPP",
                                             absoluteFilePath.c_str()};

    std::string envVar = "MYENVVAR";
    std::string envVarVal = "MYENVVARVAL";
    // Setting the last param to 0 prevents overwrite if exists
    setenv(envVar.c_str(), envVarVal.c_str(), 0);
    std::string expectedEnvVal = std::string(getenv(envVar.c_str()));

    for (const char *sourceFile : sourceFiles) {
        const char *argv[] = {"rumba",    "gcc", "-c",
                              sourceFile, "-o",  "hello.o"};
        const size_t argc = sizeof(argv) / sizeof(const char *);

        const int cmd_argc = argc - 1;
        const char **cmd_argv = &argv[1];

        std::string expectedFullCommand =
            "gcc -c " + std::string(sourceFile) + " -o hello.o";

        build::buildbox::CompilerExecutionData compilationData;
        DataUtils::collectCompilationData(cmd_argc, cmd_argv, "gcc",
                                          compilationData);

        EXPECT_EQ(compilationData.command(), "gcc");
        EXPECT_EQ(compilationData.full_command(), expectedFullCommand);
        EXPECT_EQ(compilationData.working_directory(), cwd);
        EXPECT_EQ(compilationData.environment_variables().at(envVar),
                  expectedEnvVal);
        EXPECT_EQ(compilationData.has_recc_data(), false);
        EXPECT_EQ(compilationData.source_file_info()[0].name(), sourceFile);
        EXPECT_EQ(compilationData.source_file_info()[0].digest().hash(),
                  "ae6b1296207e23a3d6a15f398bef2446b0b7173b6809afc139b9bb59128"
                  "6b7bf");
        EXPECT_EQ(compilationData.source_file_info()[0].digest().size_bytes(),
                  76);
    }
}

TEST(CollectCompilationData, CompileAndLinkCommand)
{
    clearEnv();
    std::string envVar = "MYENVVAR";
    std::string envVarVal = "MYENVVARVAL";
    // Setting the last param to 0 prevents overwrite if exists
    setenv(envVar.c_str(), envVarVal.c_str(), 0);
    std::string expectedEnvVal = std::string(getenv(envVar.c_str()));

    const char *argv[] = {"rumba",           "gcc", "data/foomain.c++",
                          "data/foodep.CPP", "-o",  "foo"};
    const size_t argc = sizeof(argv) / sizeof(const char *);
    const int cmd_argc = argc - 1;
    const char **cmd_argv = &argv[1];
    build::buildbox::CompilerExecutionData compilationData;
    DataUtils::collectCompilationData(cmd_argc, cmd_argv, "gcc",
                                      compilationData);
    EXPECT_EQ(compilationData.command(), "gcc");
    EXPECT_EQ(compilationData.full_command(),
              "gcc data/foomain.c++ data/foodep.CPP -o foo");
    EXPECT_EQ(compilationData.working_directory(),
              buildboxcommon::SystemUtils::getCurrentWorkingDirectory());
    EXPECT_EQ(compilationData.environment_variables().at(envVar),
              expectedEnvVal);
    EXPECT_EQ(compilationData.has_recc_data(), false);
    EXPECT_EQ(compilationData.source_file_info()[0].name(),
              "data/foomain.c++");
    EXPECT_EQ(
        compilationData.source_file_info()[0].digest().hash(),
        "4c85c53dbac2dfed1cedb666fb2c66f6860a46e09adcb30d87f8385db3bd29ec");
    EXPECT_EQ(compilationData.source_file_info()[0].digest().size_bytes(),
              114);
    EXPECT_EQ(compilationData.source_file_info()[1].name(), "data/foodep.CPP");
    EXPECT_EQ(
        compilationData.source_file_info()[1].digest().hash(),
        "a5a01fe1170b4a516ea8a5d7eb772a47132ae8e8267530d6bd9d2f0e22971992");
    EXPECT_EQ(compilationData.source_file_info()[1].digest().size_bytes(), 52);
}

TEST(SendData, AsExpected)
{
    clearEnv();
    setenv("RUMBA_PORT", "9080", 0);
    RUMBA_PORT = "9080";

    long portToSend = atoi(getenv("RUMBA_PORT"));

    struct sockaddr_in serverAddr;
    memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    serverAddr.sin_port = htons(atoi(getenv("RUMBA_PORT")));

    // Use a blocking socket because Linux queue lengths can be small
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd == -1) {
        // If opening the socket fails, just skip the rest of the test
        std::cerr << "Unable to use socket() for test: " << strerror(errno)
                  << std::endl;
        return;
    }

    if (bind(sockfd, (const struct sockaddr *)&serverAddr,
             sizeof(serverAddr)) == -1) {
        std::cerr << "Unable to use bind() for test: " << strerror(errno)
                  << std::endl;
        return;
    }

    // Create an CompilerExecutionData object and populate it with a fake
    // envvar
    build::buildbox::CompilerExecutionData sentData;
    (*sentData.mutable_environment_variables())["TESTENVVAR"] = "TESTENVVALUE";

    // This should be big enough to contain a mostly empty proto
    char recvbuffer[10000];

    DataUtils::sendData(sentData);
    int receivedLen =
        recvfrom(sockfd, recvbuffer, sizeof(recvbuffer), 0, 0, 0);
    if (receivedLen == -1) {
        // If the recvfrom fails, just skip the rest of the test
        std::cerr << "Unable to use recvfrom() for test" << strerror(errno)
                  << std::endl;
        return;
    }
    build::buildbox::CompilerExecutionData receivedData;
    receivedData.ParseFromArray(recvbuffer, receivedLen);
    EXPECT_EQ((*sentData.mutable_environment_variables())["TESTENVVAR"],
              (*receivedData.mutable_environment_variables())["TESTENVVAR"]);

    close(sockfd);
}

TEST(SendData, NullCharTest)
{
    clearEnv();
    setenv("RUMBA_PORT", "9080", 0);
    RUMBA_PORT = "9080";

    long portToSend = atoi(getenv("RUMBA_PORT"));

    struct sockaddr_in serverAddr;
    memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    serverAddr.sin_port = htons(atoi(getenv("RUMBA_PORT")));

    // Use a blocking socket because Linux queue lengths can be small
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd == -1) {
        // If opening the socket fails, just skip the rest of the test
        std::cerr << "Unable to use socket() for test: " << strerror(errno)
                  << std::endl;
        return;
    }

    if (bind(sockfd, (const struct sockaddr *)&serverAddr,
             sizeof(serverAddr)) == -1) {
        std::cerr << "Unable to use bind() for test: " << strerror(errno)
                  << std::endl;
        return;
    }

    // Create an CompilerExecutionData object and populate it
    build::buildbox::CompilerExecutionData sentCompilationData;
    buildCompilationData(sentCompilationData);

    // Allocate a socket read buffer
    const size_t bufferSize = 1024 * 1024 * 10;
    char *recvbuffer = new char[bufferSize];

    // Send the data
    DataUtils::sendData(sentCompilationData);
    const ssize_t receivedLen =
        recvfrom(sockfd, recvbuffer, bufferSize, 0, 0, 0);
    ASSERT_TRUE(receivedLen > 0);

    // Assert that we need to create the string buffer using `receivedLen`
    std::string truncatedStringBuffer(recvbuffer);
    std::string stringBuffer(recvbuffer, receivedLen);
    ASSERT_TRUE(truncatedStringBuffer.size() < stringBuffer.size());

    build::buildbox::CompilerExecutionData receivedData;
    receivedData.ParseFromString(stringBuffer);

    // Verify the data we recieved matches the data sent
    EXPECT_TRUE(google::protobuf::util::MessageDifferencer::Equals(
        receivedData, sentCompilationData));

    // Clean up
    close(sockfd);
    delete[] recvbuffer;
}

TEST(ParseConfig, AllSet)
{
    clearEnv();

    parseConfig("data/all-set.conf");

    EXPECT_EQ(RUMBA_LOG_DIRECTORY, "/tmp");
    EXPECT_EQ(RUMBA_VERBOSE, false);
    EXPECT_EQ(RUMBA_LOG_LEVEL, "error");
    EXPECT_EQ(RUMBA_USE_RECC, true);
    EXPECT_EQ(RUMBA_VERIFY, false);
    EXPECT_EQ(RUMBA_PORT, "18001");
    EXPECT_EQ(RUMBA_CORRELATED_INVOCATIONS_ID, "uuid345");
}

TEST(ParseConfig, AllSetBinary)
{
    clearEnv();

    parseConfig("data/all-set-binary.conf");

    EXPECT_EQ(RUMBA_LOG_DIRECTORY, "/tmp");
    EXPECT_EQ(RUMBA_VERBOSE, false);
    EXPECT_EQ(RUMBA_LOG_LEVEL, "error");
    EXPECT_EQ(RUMBA_USE_RECC, true);
    EXPECT_EQ(RUMBA_VERIFY, false);
    EXPECT_EQ(RUMBA_PORT, "18001");
    EXPECT_EQ(RUMBA_CORRELATED_INVOCATIONS_ID, "uuid345");
}

TEST(ParseConfig, AllSetCaps)
{
    clearEnv();

    parseConfig("data/all-set-caps.conf");

    EXPECT_EQ(RUMBA_LOG_DIRECTORY, "/tmp");
    EXPECT_EQ(RUMBA_VERBOSE, false);
    EXPECT_EQ(RUMBA_LOG_LEVEL, "error");
    EXPECT_EQ(RUMBA_USE_RECC, true);
    EXPECT_EQ(RUMBA_VERIFY, false);
    EXPECT_EQ(RUMBA_PORT, "18001");
    EXPECT_EQ(RUMBA_CORRELATED_INVOCATIONS_ID, "uuid345");
}

TEST(ParseConfig, AllSetAllLowercase)
{
    clearEnv();

    parseConfig("data/all-set-lowercase.conf");

    EXPECT_EQ(RUMBA_LOG_DIRECTORY, "/tmp");
    EXPECT_EQ(RUMBA_VERBOSE, false);
    EXPECT_EQ(RUMBA_LOG_LEVEL, "error");
    EXPECT_EQ(RUMBA_USE_RECC, true);
    EXPECT_EQ(RUMBA_VERIFY, false);
    EXPECT_EQ(RUMBA_PORT, "18001");
    EXPECT_EQ(RUMBA_CORRELATED_INVOCATIONS_ID, "uuid345");
}

TEST(ParseConfig, AllSetWeirdSpacing)
{
    clearEnv();

    parseConfig("data/all-set-spacing.conf");

    EXPECT_EQ(RUMBA_LOG_DIRECTORY, "/tmp");
    EXPECT_EQ(RUMBA_VERBOSE, false);
    EXPECT_EQ(RUMBA_LOG_LEVEL, "error");
    EXPECT_EQ(RUMBA_USE_RECC, true);
    EXPECT_EQ(RUMBA_VERIFY, false);
    EXPECT_EQ(RUMBA_PORT, "18001");
    EXPECT_EQ(RUMBA_CORRELATED_INVOCATIONS_ID, "uuid345");
}

TEST(ParseConfig, AllSetWithComment)
{
    clearEnv();

    parseConfig("data/all-set-comment.conf");

    EXPECT_EQ(RUMBA_LOG_DIRECTORY, "/tmp");
    EXPECT_EQ(RUMBA_VERBOSE, false);
    EXPECT_EQ(RUMBA_LOG_LEVEL, "error");
    EXPECT_EQ(RUMBA_USE_RECC, true);
    EXPECT_EQ(RUMBA_VERIFY, false);
    EXPECT_EQ(RUMBA_PORT, "18001");
    EXPECT_EQ(RUMBA_CORRELATED_INVOCATIONS_ID, "uuid345");
}

TEST(ParseConfig, AllSetOverride)
{
    clearEnv();

    setenv("RUMBA_VERBOSE", "true", 0);

    parseConfig("data/all-set.conf");

    EXPECT_EQ(RUMBA_LOG_DIRECTORY, "/tmp");
    EXPECT_EQ(RUMBA_VERBOSE, true);
    EXPECT_EQ(RUMBA_LOG_LEVEL, "error");
    EXPECT_EQ(RUMBA_USE_RECC, true);
    EXPECT_EQ(RUMBA_VERIFY, false);
    EXPECT_EQ(RUMBA_PORT, "18001");
    EXPECT_EQ(RUMBA_CORRELATED_INVOCATIONS_ID, "uuid345");
}

TEST(ParseConfig, AllSetOverrideCaps)
{
    clearEnv();

    setenv("RUMBA_VERBOSE", "TRUE", 0);

    parseConfig("data/all-set.conf");

    EXPECT_EQ(RUMBA_LOG_DIRECTORY, "/tmp");
    EXPECT_EQ(RUMBA_VERBOSE, true);
    EXPECT_EQ(RUMBA_LOG_LEVEL, "error");
    EXPECT_EQ(RUMBA_USE_RECC, true);
    EXPECT_EQ(RUMBA_VERIFY, false);
    EXPECT_EQ(RUMBA_PORT, "18001");
    EXPECT_EQ(RUMBA_CORRELATED_INVOCATIONS_ID, "uuid345");
}

TEST(ParseConfig, AllSetOverrideBinary)
{
    clearEnv();

    setenv("RUMBA_VERBOSE", "1", 0);

    parseConfig("data/all-set.conf");

    EXPECT_EQ(RUMBA_LOG_DIRECTORY, "/tmp");
    EXPECT_EQ(RUMBA_VERBOSE, true);
    EXPECT_EQ(RUMBA_LOG_LEVEL, "error");
    EXPECT_EQ(RUMBA_USE_RECC, true);
    EXPECT_EQ(RUMBA_VERIFY, false);
    EXPECT_EQ(RUMBA_PORT, "18001");
    EXPECT_EQ(RUMBA_CORRELATED_INVOCATIONS_ID, "uuid345");
}

TEST(ParseConfig, IncorrectVerify)
{
    clearEnv();

    parseConfig("data/incorrect-verify.conf");

    EXPECT_EQ(RUMBA_LOG_DIRECTORY, "/tmp");
    EXPECT_EQ(RUMBA_VERBOSE, false);
    EXPECT_EQ(RUMBA_LOG_LEVEL, "error");
    EXPECT_EQ(RUMBA_USE_RECC, false);
    EXPECT_EQ(RUMBA_VERIFY, false); // Rumba corrects this to false when
                                    // verify=true but use_recc=false
    EXPECT_EQ(RUMBA_PORT, "18001");
    EXPECT_EQ(RUMBA_CORRELATED_INVOCATIONS_ID, "uuid345");
}

TEST(ParseConfig, OnlyVerbose)
{
    clearEnv();

    parseConfig("data/only-verbose.conf");

    EXPECT_EQ(RUMBA_LOG_DIRECTORY, "");
    EXPECT_EQ(RUMBA_VERBOSE, true);
    EXPECT_EQ(RUMBA_LOG_LEVEL, "error");
    EXPECT_EQ(RUMBA_USE_RECC, false);
    EXPECT_EQ(RUMBA_VERIFY, false);
    EXPECT_EQ(RUMBA_PORT, "19111");
    // RUMBA_CORRELATED_INVOCATIONS_ID will be random by default, can't verify
}

TEST(ParseConfig, NoFile)
{
    clearEnv();

    parseConfig("");

    EXPECT_EQ(RUMBA_LOG_DIRECTORY, "");
    EXPECT_EQ(RUMBA_VERBOSE, false);
    EXPECT_EQ(RUMBA_LOG_LEVEL, "error");
    EXPECT_EQ(RUMBA_USE_RECC, false);
    EXPECT_EQ(RUMBA_VERIFY, false);
    EXPECT_EQ(RUMBA_PORT, "19111");
    // RUMBA_CORRELATED_INVOCATIONS_ID will be random by default, can't verify
}

TEST(ParseConfig, NoFileWithEnv)
{
    clearEnv();

    setenv("RUMBA_USE_RECC", "true", 0);

    parseConfig("");

    EXPECT_EQ(RUMBA_LOG_DIRECTORY, "");
    EXPECT_EQ(RUMBA_VERBOSE, false);
    EXPECT_EQ(RUMBA_LOG_LEVEL, "error");
    EXPECT_EQ(RUMBA_USE_RECC, true);
    EXPECT_EQ(RUMBA_VERIFY, false);
    EXPECT_EQ(RUMBA_PORT, "19111");
    // RUMBA_CORRELATED_INVOCATIONS_ID will be random by default, can't verify
}

} // namespace test
} // namespace rumba
