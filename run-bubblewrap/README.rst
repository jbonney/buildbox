buildbox-run-bubblewrap
=======================

An implementation of a runner for BuildGrid/buildbox using `Bubblewrap
<https://github.com/containers/bubblewrap>`_, a tool that allows
creating unprivileged chroot-like environments with Linux user
namespaces.

To run, ``bwrap`` is expected to be in ``$PATH``.

Dependencies
~~~~~~~~~~~~

Ensure that you have bubblewrap installed. In Debian/Ubuntu, this can be done
with::

    sudo apt install bubblewrap

Usage
~~~~~
::

    usage: ./buildbox-run-bubblewrap [OPTIONS]
        --action=PATH               Path to read input Action from
        --action-result=PATH        Path to write output ActionResult to
        --log-level=LEVEL           (default: info) Log verbosity: trace/debug/info/warning/error
        --verbose                   Set log level to debug
        --log-file=FILE             File to write log to
        --use-localcas              Use LocalCAS protocol methods (default behavior)
                                    NOTE: this option will be deprecated.
        --disable-localcas          Do not use LocalCAS protocol methods
        --workspace-path=PATH       Location on disk which runner will use as root when executing jobs
        --stdout-file=FILE          File to redirect the command's stdout to
        --stderr-file=FILE          File to redirect the command's stderr to
        --no-logs-capture           Do not capture and upload the contents written to stdout and stderr
        --capabilities              Print capabilities supported by this runner
        --remote=URL                URL for CAS service
        --instance=NAME             Name of the CAS instance
        --server-cert=PATH          Public server certificate for TLS (PEM-encoded)
        --client-key=PATH           Private client key for TLS (PEM-encoded)
        --client-cert=PATH          Public client certificate for TLS (PEM-encoded)
        --retry-limit=INT           Number of times to retry on grpc errors
        --retry-delay=MILLISECONDS  How long to wait before the first grpc retry
        --bind-mount=HOSTPATH:PATH  Bind mount file or directory from host into sandbox
