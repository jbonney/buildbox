/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxrun_bubblewrap.h>

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_mergeutil.h>
#include <buildboxcommon_platformutils.h>
#include <buildboxcommon_remoteexecutionclient.h>
#include <buildboxcommon_runner.h>
#include <buildboxcommon_systemutils.h>

#include <filesystem>
#include <stdlib.h>
#include <sys/utsname.h>
#include <unistd.h>

/* Not defined in any header per POSIX */
extern "C" char **environ;

namespace buildboxcommon {
namespace buildboxrun {
namespace bubblewrap {

/**
 * Check Platform properties for the chroot root digest,
 * ensure that's not empty and populate output parameter 'chrootDigest'
 */
bool isMergeRequest(const Platform &platform, Digest *chrootDigest)
{
    static const char chrootPropName[] = "chrootRootDigest";
    for (const auto &property : platform.properties()) {
        if (property.name() == chrootPropName) {
            const std::string &hash = property.value();
            if (hash.empty()) {
                BUILDBOXCOMMON_THROW_EXCEPTION(
                    std::runtime_error, "Found invalid '"
                                            << chrootPropName
                                            << "' value with an empty hash");
            }

            const char *slash = strchr(hash.c_str(), '/');
            if (slash == nullptr) {
                BUILDBOXCOMMON_THROW_EXCEPTION(
                    std::runtime_error,
                    "Found invalid '"
                        << chrootPropName
                        << "' value with valid hash, but no blob size");
            }

            chrootDigest->set_hash(
                std::string(hash.c_str(),
                            static_cast<unsigned long>(slash - hash.c_str())));
            chrootDigest->set_size_bytes(std::stoll(slash + 1));
            return true;
        }
    }

    return false;
}

void BubbleWrapRunner::fetchChrootIntoLocalCas(const Digest &digest) const
{
    BUILDBOX_RUNNER_LOG(INFO, "Calling LocalCAS.FetchTree(chrootDigest="
                                  << digest
                                  << ") to make sure that the chroot is "
                                     "available from Local CAS");

    try {
        const bool fetchFiles = true;
        d_casClient->fetchTree(digest, fetchFiles);
    }
    catch (const buildboxcommon::GrpcError &e) {
        BUILDBOX_RUNNER_LOG(ERROR, "LocalCAS.FetchTree(chrootDigest="
                                       << digest
                                       << ") failed: " << e.status.error_code()
                                       << ": " << e.status.error_message());
        throw e;
    }
}

void BubbleWrapRunner::uploadMissingBlobs(const std::vector<Digest> &digests,
                                          const digest_string_map &data) const
{
    auto casClient = d_casClient;

    const std::vector<Digest> missingDigests =
        casClient->findMissingBlobs(digests);

    if (missingDigests.empty()) {
        return;
    }

    BUILDBOX_RUNNER_LOG(DEBUG, "Uploading " << missingDigests.size()
                                            << " missing blob(s) out of "
                                            << digests.size() << " total");

    std::vector<CASClient::UploadRequest> uploadRequests;
    uploadRequests.reserve(missingDigests.size());
    for (const auto &digest : missingDigests) {
        uploadRequests.emplace_back(digest, data.at(digest));
    }

    const std::vector<CASClient::UploadResult> results =
        casClient->uploadBlobs(uploadRequests);

    // verify that all blobs have been successfully uploaded
    // throw on any failures
    bool uploadSuccess = true;
    std::ostringstream oss;
    for (const auto &result : results) {
        if (!result.status.ok()) {
            oss << "Failed to upload a merged digest(" << result.digest
                << "), status = [" << result.status.error_code() << ": \""
                << result.status.error_message() << "\"]\n";
            uploadSuccess = false;
        }
    }

    if (!uploadSuccess) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error, oss.str());
    }
}

Digest BubbleWrapRunner::mergeTrees(const MergeUtil::DirectoryTree &inputTree,
                                    const MergeUtil::DirectoryTree &chrootTree,
                                    digest_string_map *mergedDirectoryBlobs,
                                    MergeUtil::DigestVector *newDigests)
{
    Digest mergedRootDigest;
    const bool success =
        MergeUtil::createMergedDigest(inputTree, chrootTree, &mergedRootDigest,
                                      mergedDirectoryBlobs, newDigests);
    if (success) {
        return mergedRootDigest;
    }
    else {
        return Digest();
    }
}

void BubbleWrapRunner::mergeDigests(const Digest &inputDigest,
                                    const Digest &chrootDigest,
                                    Digest *mergedRootDigest)
{
    // acquire the input trees of both digests
    // getTree will throw on error
    const MergeUtil::DirectoryTree inputTree =
        this->d_casClient->getTree(inputDigest);

    const MergeUtil::DirectoryTree chrootTree =
        this->d_casClient->getTree(chrootDigest);

    digest_string_map mergedDirectoryBlobs;
    MergeUtil::DigestVector newDigests;
    const Digest rootDigest =
        mergeTrees(inputTree, chrootTree, &mergedDirectoryBlobs, &newDigests);

    if (rootDigest == Digest()) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error, "Error merging inputDigest("
                                    << inputDigest << ") with chrootDigest("
                                    << chrootDigest << ")");
    }

    // Upload any missing digests to CAS so they can be staged
    uploadMissingBlobs(newDigests, mergedDirectoryBlobs);

    *mergedRootDigest = rootDigest;
}

BubbleWrapRunner::BubbleWrapRunner(const std::string &bwrap_path) : Runner()
{
    if (bwrap_path != "") {
        d_bubblewrapBinPath = bwrap_path;
    }
    else {
        const std::string bubblewrapBin = "bwrap";

        /* Find the path to the bubblewrap binary. */
        d_bubblewrapBinPath = SystemUtils::getPathToCommand(bubblewrapBin);
        if (d_bubblewrapBinPath.empty()) {
            std::cerr << "Could not find bubblewrap command \""
                      << bubblewrapBin << "\"\n";
            exit(1);
        }
    }

    d_linux32Path = SystemUtils::getPathToCommand("linux32");

    d_userNamespaceAvailable = userNamespaceAvailable();

    d_enableTmpOutputs = false;
}

bool BubbleWrapRunner::userNamespaceAvailable()
{
    const auto maxUserNamespacesPath = "/proc/sys/user/max_user_namespaces";

    if (!FileUtils::isRegularFile("/proc/self/ns/user") ||
        !FileUtils::isRegularFile(maxUserNamespacesPath)) {
        /* Kernel too old or user namespace support disabled in config */
        return false;
    }

    /* Check whether user namespaces are disabled by the distro or admin */
    const auto maxUserNamespaces =
        FileUtils::getFileContents(maxUserNamespacesPath);
    return maxUserNamespaces != "0\n";
}

const std::vector<std::string>
BubbleWrapRunner::generateCommandLine(const Command &command,
                                      const std::string &root_path,
                                      const Platform &platform)
{
    std::vector<std::string> bwrap_argv;

    std::string uid("0");
    std::string gid("0");
    bool network = false;
    bool linux32 = false;

    /* Process platform properties */
    for (const auto &property : platform.properties()) {
        if (property.name() == "OSFamily") {
            if (property.value() != PlatformUtils::getHostOSFamily()) {
                std::cerr << "Unsupported OSFamily \"" << property.value()
                          << "\"\n";
                exit(1);
            }
        }
        else if (property.name() == "ISA") {
            auto support = PlatformUtils::getISASupport(property.value());
            bool supported = false;

            if (support.supported) {
                switch (support.action) {
                    case PlatformUtils::ISA_SUPPORT_ACTION_NONE:
                        supported = true;
                        break;
                    case PlatformUtils::ISA_SUPPORT_ACTION_RUN_LINUX32:
                        supported = true;
                        linux32 = true;
                        break;
                    default:
                        break;
                }
            }
            if (!supported) {
                std::cerr << "Unsupported ISA \"" << property.value()
                          << "\"\n";
                exit(1);
            }
        }
        else if (property.name() == "unixUID") {
            if (!d_userNamespaceAvailable) {
                std::cerr << "User namespaces are not available. "
                             "Cannot support unixUID.\n";
                exit(1);
            }
            uid = property.value();
        }
        else if (property.name() == "unixGID") {
            if (!d_userNamespaceAvailable) {
                std::cerr << "User namespaces are not available. "
                             "Cannot support unixGID.\n";
                exit(1);
            }
            gid = property.value();
        }
        else if (property.name() == "network") {
            network = property.value() == "on";
        }
    }

    if (linux32) {
        if (d_linux32Path.empty()) {
            std::cerr << "Could not find linux32 command\n";
            exit(1);
        }

        bwrap_argv.push_back(d_linux32Path);
    }

    bwrap_argv.push_back(d_bubblewrapBinPath);

    /* Create a new pid namespace, this also ensures that any subprocesses
     * are cleaned up when the bwrap process exits.
     */
    bwrap_argv.push_back("--unshare-pid");

    /* Ensure subprocesses are cleaned up when the bwrap parent dies. */
    bwrap_argv.push_back("--die-with-parent");

    /* Mount sandbox rootfs */
    bwrap_argv.push_back("--bind");
    bwrap_argv.push_back(root_path);
    bwrap_argv.push_back("/");

    if (!network) {
        /* Disable network access */
        bwrap_argv.push_back("--unshare-net");
        bwrap_argv.push_back("--unshare-uts");
        bwrap_argv.push_back("--hostname");
        bwrap_argv.push_back("buildbox");
        bwrap_argv.push_back("--unshare-ipc");
    }

    if (command.working_directory().compare("") != 0) {
        bwrap_argv.push_back("--dir");
        bwrap_argv.push_back(command.working_directory());
        bwrap_argv.push_back("--chdir");
        bwrap_argv.push_back(command.working_directory());
    }
    else {
        bwrap_argv.push_back("--chdir");
        bwrap_argv.push_back("/");
    }

    if (d_userNamespaceAvailable) {
        bwrap_argv.push_back("--unshare-user");
        bwrap_argv.push_back("--uid");
        bwrap_argv.push_back(uid);
        bwrap_argv.push_back("--gid");
        bwrap_argv.push_back(gid);
    }

    /* Clear environment */
    for (char **envp = environ; *envp; envp++) {
        const char *assign = strchr(*envp, '=');
        assert(assign);
        const std::string key(*envp, static_cast<size_t>(assign - *envp));

        bwrap_argv.push_back("--unsetenv");
        bwrap_argv.push_back(key);
    }

    /* Set environment for command process */
    for (const auto &env_var : command.environment_variables()) {
        bwrap_argv.push_back("--setenv");
        bwrap_argv.push_back(env_var.name());
        bwrap_argv.push_back(env_var.value());
    }

    /* Support proc */
    bwrap_argv.push_back("--proc");
    bwrap_argv.push_back("/proc");

    /* If /tmp has no contents and isn't referenced by an
       output path, mount it as tmpfs.
    */
    bool tmpCapture = false;
    if (d_enableTmpOutputs) {
        for (const auto &path : command.output_paths()) {
            const auto fullOutputPath = (std::filesystem::path{"/"} /
                                         command.working_directory() / path)
                                            .lexically_normal();
            // Skip past '/'
            const auto top_dir = ++(fullOutputPath.begin());
            if (*top_dir == "tmp") {
                tmpCapture = true;
                break;
            }
        }
    }
    const auto tmpPath = std::filesystem::path{root_path} / "tmp";
    if ((std::filesystem::is_directory(tmpPath) &&
         !std::filesystem::is_empty(tmpPath)) ||
        tmpCapture) {
        /* /tmp is used as input and/or output, use a regular directory */
        bwrap_argv.push_back("--dir");
        bwrap_argv.push_back("/tmp");
    }
    else {
        bwrap_argv.push_back("--tmpfs");
        bwrap_argv.push_back("/tmp");
    }

    /* Support devices */
    bwrap_argv.push_back("--dev");
    bwrap_argv.push_back("/dev");

    bwrap_argv.insert(bwrap_argv.end(), this->d_bindArguments.cbegin(),
                      this->d_bindArguments.cend());

    /* append command */
    auto &arguments = command.arguments();
    bwrap_argv.insert(bwrap_argv.end(), arguments.begin(), arguments.end());

    return bwrap_argv;
}

bool BubbleWrapRunner::parseArg(const char *arg)
{
    if (arg[0] == '-' && arg[1] == '-') {
        arg += 2;
        const char *assign = strchr(arg, '=');
        if (assign) {
            const std::string key(arg, static_cast<size_t>(assign - arg));
            const char *value = assign + 1;
            if (key == "bind-mount") {
                const char *colon = strchr(value, ':');
                if (!colon) {
                    std::cerr << "Missing `:` in option " << arg << std::endl;
                    return false;
                }

                std::string src(value, static_cast<size_t>(colon - value));
                std::string dest(colon + 1);

                /* Use --dev-bind to allow binding device nodes.
                 * It can be used for regular files and directories as
                 * well.
                 */
                this->d_bindArguments.push_back("--dev-bind");
                this->d_bindArguments.push_back(src);
                this->d_bindArguments.push_back(dest);

                return true;
            }
        }
        else {
            const std::string key(arg);
            if (key == "enable-tmp-outputs") {
                this->d_enableTmpOutputs = true;
                return true;
            }
        }
    }

    return false;
}

void BubbleWrapRunner::printSpecialUsage()
{
    std::clog << "    --bind-mount=HOSTPATH:PATH  Bind mount file or "
                 "directory from host into sandbox\n";
    std::clog << "    --enable-tmp-outputs  Allow capturing outputs under "
                 "/tmp directory in the sandbox\n";
}

void BubbleWrapRunner::printSpecialCapabilities()
{
    std::unordered_set<std::string> supported;
    std::cout << "bind-mount\n";
    std::cout << "platform:OSFamily=" << PlatformUtils::getHostOSFamily()
              << "\n";

    supported = PlatformUtils::getSupportedISAs();
    for (const auto &isa : supported) {
        std::cout << "platform:ISA=" << isa << "\n";
    }

    if (d_userNamespaceAvailable) {
        std::cout << "platform:unixUID\n";
        std::cout << "platform:unixGID\n";
    }
}

ActionResult BubbleWrapRunner::execute(const Command &command,
                                       const Digest &inputRootDigest,
                                       const Platform &platform)
{
    Digest digest(inputRootDigest);
    Digest chrootDigest;
    ActionResult result;

    auto *result_metadata = result.mutable_execution_metadata();
    Runner::metadata_mark_input_download_start(result_metadata);
    if (isMergeRequest(commandPlatformDeprecated(command), &chrootDigest)) {
        if (d_use_localcas_protocol) {
            // Making sure that the chroot is pre-loaded in local CAS if
            // connected to one.
            fetchChrootIntoLocalCas(chrootDigest);
        }

        Digest mergedDigest;
        mergeDigests(inputRootDigest, chrootDigest, &mergedDigest);
        BUILDBOX_RUNNER_LOG(DEBUG, "inputRootDigest = "
                                       << inputRootDigest
                                       << ", chrootDigest = " << chrootDigest
                                       << ", mergedDigest = " << mergedDigest);
        digest.Swap(&mergedDigest);
    }

    const auto staged_dir = this->stageDirectory(digest);
    Runner::metadata_mark_input_download_end(result_metadata);

    std::ostringstream working_directory;
    working_directory << staged_dir->getPath() << "/"
                      << command.working_directory();
    BUILDBOX_LOG_DEBUG("Running in " << working_directory.str());

    {
        createOutputDirectories(command, working_directory.str());
        std::vector<std::string> commandLine =
            generateCommandLine(command, staged_dir->getPath(), platform);
        BUILDBOX_LOG_DEBUG("Executing "
                           << logging::printableCommandLine(commandLine))
        executeAndStore(commandLine, &result);
    }

    if (!getSignalStatus()) {
        BUILDBOX_LOG_DEBUG("Capturing command outputs...");
        Runner::metadata_mark_output_upload_start(result_metadata);
        staged_dir->captureAllOutputs(command, &result);
        Runner::metadata_mark_output_upload_end(result_metadata);
        BUILDBOX_LOG_DEBUG("Finished capturing command outputs");
    }

    return result;
}

} // namespace bubblewrap
} // namespace buildboxrun
} // namespace buildboxcommon
