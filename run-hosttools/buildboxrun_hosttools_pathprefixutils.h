/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_HOSTTOOLS_PATHPREFIXUTILS
#define INCLUDED_HOSTTOOLS_PATHPREFIXUTILS

#include <string>
#include <vector>

namespace buildboxcommon {
namespace buildboxrun {
namespace hosttools {

class PathPrefixUtils {
  public:
    /**
     * Find all absolute paths in the compile command and prefix
     * them with the location of the staged directory
     */
    static std::vector<std::string>
    prefixAbspathsWithStagedDir(const std::vector<std::string> &command,
                                const std::string &staged_directory);

    /**
     * Given a single argument, prefix it with the staged directory.
     * This will handle certain compile commands like -I/foo, properly
     * prefixing it as -Idir/foo
     */
    static const std::string
    prefixArgWithStagedDir(const std::string &argument,
                           const std::string &staged_dir);
};

} // namespace hosttools
} // namespace buildboxrun
} // namespace buildboxcommon

#endif
