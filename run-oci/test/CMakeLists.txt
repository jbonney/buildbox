include(${CMAKE_SOURCE_DIR}/cmake/BuildboxGTestSetup.cmake)

if (${CMAKE_SYSTEM_NAME} EQUAL "Linux")
    add_executable(run_oci_tests
        oci.t.cpp
        run_subprocess.t.cpp
        ../buildboxrun_oci.cpp
        ../buildboxrun_run_subprocess.cpp
        ../buildboxrun_container_host.cpp
    )
    target_precompile_headers(run_oci_tests REUSE_FROM common)

    target_include_directories(run_oci_tests PRIVATE "..")
    target_link_libraries(run_oci_tests common nlohmann_json::nlohmann_json
        ${GTEST_MAIN_TARGET} ${GTEST_TARGET})

    add_test(NAME run_oci_tests COMMAND run_oci_tests)
endif()
