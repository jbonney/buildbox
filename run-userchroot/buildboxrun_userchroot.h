/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXRUN_USERCHROOT
#define INCLUDED_BUILDBOXRUN_USERCHROOT

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_mergeutil.h>
#include <buildboxcommon_runner.h>
#include <buildboxcommon_systemutils.h>

namespace buildboxcommon {
namespace buildboxrun {
namespace userchroot {

class UserChrootRunner : public Runner {
  public:
    /**
     * Run a command in a userchroot sandbox.
     */
    ActionResult execute(const Command &command, const Digest &inputRootDigest,
                         const Platform &platform) override;

    // Parse custom arguments for this runner
    bool parseArg(const char *arg) override;

    // Print custom usage
    virtual void printSpecialUsage() override;

  private:
    // location of userchroot binary
    std::string d_userchroot_bin = "userchroot";

    // Vector of host paths to copy/hardlink into the chroot at construction
    // time. Contents will be strings of the form `/host/path:/chroot/path`.
    std::vector<std::string> d_mappedPaths;

    // location of sudo binary
    std::string d_sudoBin = "sudo";
    // Run `userchroot` as this user, i.e., sudo -u
    std::string d_user;
    // Run `userchroot` as this group, i.e., sudo -g
    std::string d_group;

    void mergeDigests(const Digest &inputDigest, const Digest &chrootDigest,
                      Digest *mergedRootDigest);

    void uploadMissingBlobs(const std::vector<Digest> &digests,
                            const digest_string_map &data) const;

    static Digest mergeTrees(const MergeUtil::DirectoryTree &inputTree,
                             const MergeUtil::DirectoryTree &chrootTree,
                             digest_string_map *mergedDirectoryBlobs,
                             MergeUtil::DigestVector *newDigests);

    ProcessCredentials getProcessCredentials() const;

    // Optional. If set, blobs in the merged chroot will be uploaded using this
    // instance name.
    // (This is useful in order to avoid those blobs from being forwarded to a
    // remote in case of being connected to a casd proxy instance.)
    std::string d_localOnlyCasInstanceName;
    ConnectionOptions d_casRemote;

    // This mode will be set in the root directory of the merged trees.
    // That field will signal casd to chmod() the root of the staged
    // directory with a value that allows userchroot to work.
    static const google::protobuf::uint32 s_rootDirectoryRequiredUnixMode =
        0755;

    // Call `LocalCAS.FetchTree()` for the given Digest.
    void fetchChrootIntoLocalCas(const buildboxcommon::Digest &digest) const;

  protected:
    // (Methods marked `protected` for unit testing).

    // Certain directories must be present in all chroots.
    // Check whether that is the case by looking in the staged directory
    // and otherwise throw.
    void assertRequiredDirectoriesExist() const;

    // Attempt to merge two trees. The root of the result will have `UnixMode`
    // set in its node properties.
    // On errors returns an empty Digest.
    static Digest mergeTreesAndSetRootDirectoryProperty(
        const MergeUtil::DirectoryTree &inputTree,
        const MergeUtil::DirectoryTree &chrootTree,
        digest_string_map *mergedDirectoryBlobs,
        MergeUtil::DigestVector *newDigests);
};

} // namespace userchroot
} // namespace buildboxrun
} // namespace buildboxcommon
#endif
