#!/bin/sh

CALLS=$(head -n 1 userchrootmock_storage.txt)

if [ "$2" = "--install-devices" ]
then
    ITER=$(expr $CALLS + 1)
    echo $ITER > userchrootmock_storage.txt
elif [ "$2" = "--uninstall-devices" ] && [ $CALLS -ge 1 ]
then
    echo 0 > userchrootmock_storage.txt
else
    exit 1
fi
