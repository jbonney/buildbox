# trexe

Lightweight cross-platform remote execution client


```
Usage: trexe
   --help                                Display usage and exit.
   --remote                              URL for all services [optional]
   --instance                            Instance for all services [optional]
   --server-cert                         Server TLS certificate for all services (PEM-encoded) [optional]
   --client-key                          Client private TLS key far all services (PEM-encoded) [optional]
   --client-cert                         Client TLS certificate for all services (PEM-encoded) [optional]
   --access-token                        Authentication token for all services (JWT, OAuth token etc), will be included as an HTTP Authorization bearer token [optional]
   --token-reload-interval               Default access token refresh timeout [optional]
   --googleapi-auth                      Use GoogleAPIAuth for all services [optional]
   --retry-limit                         Retry limit for gRPC errors for all services [optional]
   --retry-delay                         Retry delay for gRPC errors for all services [optional]
   --request-timeout                     Timeout for gRPC requests for all services  (set to 0 to disable timeout) [optional]
   --min-throughput                      Minimum throughput for gRPC requests for all services, bytes per seconds.
                                            The value may be suffixed with K, M, G or T. [optional]
   --keepalive-time                      gRPC keepalive pings period for all services (set to 0 to disable keepalive pings) [optional]
   --load-balancing-policy               gRPC load balancing policy for all services (valid options are 'round_robin' and 'grpclb') [optional]
   --cas-remote                          URL for the CAS service [optional]
   --cas-instance                        Name of the CAS instance [optional]
   --cas-server-cert                     Server TLS certificate for CAS (PEM-encoded) [optional]
   --cas-client-key                      Client private TLS key for CAS (PEM-encoded) [optional]
   --cas-client-cert                     Client TLS certificate for CAS (PEM-encoded) [optional]
   --cas-access-token                    Authentication token for CAS (JWT, OAuth token etc), will be included as an HTTP Authorization bearer token [optional]
   --cas-token-reload-interval           Access token refresh timeout for CAS service [optional]
   --cas-googleapi-auth                  Use GoogleAPIAuth for CAS service [optional]
   --cas-retry-limit                     Retry limit for gRPC errors for CAS service [optional]
   --cas-retry-delay                     Retry delay for gRPC errors for CAS service [optional]
   --cas-request-timeout                 Timeout for gRPC requests for CAS service (set to 0 to disable timeout) [optional]
   --cas-min-throughput                  Minimum throughput for gRPC requests for CAS service, bytes per seconds.
                                            The value may be suffixed with K, M, G or T. [optional]
   --cas-keepalive-time                  gRPC keepalive pings period for CAS service (set to 0 to disable keepalive pings) [optional]
   --cas-load-balancing-policy           gRPC load balancing policy for CAS service (valid options are 'round_robin' and 'grpclb') [optional]
   --ac-remote                           URL for the ActionCache service [optional]
   --ac-instance                         Name of the ActionCache instance [optional]
   --ac-server-cert                      Server TLS certificate for ActionCache (PEM-encoded) [optional]
   --ac-client-key                       Client private TLS key for ActionCache (PEM-encoded) [optional]
   --ac-client-cert                      Client TLS certificate for ActionCache (PEM-encoded) [optional]
   --ac-access-token                     Authentication token for ActionCache (JWT, OAuth token etc), will be included as an HTTP Authorization bearer token [optional]
   --ac-token-reload-interval            Access token refresh timeout for ActionCache service [optional]
   --ac-googleapi-auth                   Use GoogleAPIAuth for ActionCache service [optional]
   --ac-retry-limit                      Retry limit for gRPC errors for ActionCache service [optional]
   --ac-retry-delay                      Retry delay for gRPC errors for ActionCache service [optional]
   --ac-request-timeout                  Timeout for gRPC requests for ActionCache service (set to 0 to disable timeout) [optional]
   --ac-min-throughput                   Minimum throughput for gRPC requests for ActionCache service, bytes per seconds.
                                            The value may be suffixed with K, M, G or T. [optional]
   --ac-keepalive-time                   gRPC keepalive pings period for ActionCache service (set to 0 to disable keepalive pings) [optional]
   --ac-load-balancing-policy            gRPC load balancing policy for ActionCache service (valid options are 'round_robin' and 'grpclb') [optional]
   --exec-remote                         URL for the Execution service [optional]
   --exec-instance                       Name of the Execution instance [optional]
   --exec-server-cert                    Server TLS certificate for Execution (PEM-encoded) [optional]
   --exec-client-key                     Client private TLS key for Execution (PEM-encoded) [optional]
   --exec-client-cert                    Client TLS certificate for Execution (PEM-encoded) [optional]
   --exec-access-token                   Authentication token for Execution (JWT, OAuth token etc), will be included as an HTTP Authorization bearer token [optional]
   --exec-token-reload-interval          Access token refresh timeout for Execution service [optional]
   --exec-googleapi-auth                 Use GoogleAPIAuth for Execution service [optional]
   --exec-retry-limit                    Retry limit for gRPC errors for Execution service [optional]
   --exec-retry-delay                    Retry delay for gRPC errors for Execution service [optional]
   --exec-request-timeout                Timeout for gRPC requests for Execution service (set to 0 to disable timeout) [optional]
   --exec-min-throughput                 Minimum throughput for gRPC requests for Execution service, bytes per seconds.
                                            The value may be suffixed with K, M, G or T. [optional]
   --exec-keepalive-time                 gRPC keepalive pings period for Execution service (set to 0 to disable keepalive pings) [optional]
   --exec-load-balancing-policy          gRPC load balancing policy for Execution service (valid options are 'round_robin' and 'grpclb') [optional]
   --logstream-remote                    URL for the LogStream service [optional]
   --logstream-instance                  Name of the LogStream instance [optional]
   --logstream-server-cert               Server TLS certificate for LogStream (PEM-encoded) [optional]
   --logstream-client-key                Client private TLS key for LogStream (PEM-encoded) [optional]
   --logstream-client-cert               Client TLS certificate for LogStream (PEM-encoded) [optional]
   --logstream-access-token              Authentication token for LogStream (JWT, OAuth token etc), will be included as an HTTP Authorization bearer token [optional]
   --logstream-token-reload-interval     Access token refresh timeout for LogStream service [optional]
   --logstream-googleapi-auth            Use GoogleAPIAuth for LogStream service [optional]
   --logstream-retry-limit               Retry limit for gRPC errors for LogStream service [optional]
   --logstream-retry-delay               Retry delay for gRPC errors for LogStream service [optional]
   --logstream-request-timeout           Timeout for gRPC requests for LogStream service (set to 0 to disable timeout) [optional]
   --logstream-min-throughput            Minimum throughput for gRPC requests for LogStream service, bytes per seconds.
                                            The value may be suffixed with K, M, G or T. [optional]
   --logstream-keepalive-time            gRPC keepalive pings period for LogStream service (set to 0 to disable keepalive pings) [optional]
   --logstream-load-balancing-policy     gRPC load balancing policy for LogStream service (valid options are 'round_robin' and 'grpclb') [optional]
   --config-file                         Config file to specify all options in TOML. [optional]
   --d                                   Download completed action outputs to this path. Leave empty for no download. [optional]
   --working-dir                         Path to working directory. [optional]
   --input-root-digest                   Input root digest for Action.  [optional]
   --input-path                          Input path for Action. --input-path=<path> for each input. Use `local-path:remote-path` to map an input to a specific path remotely [optional]
   --output-path                         Output path to capture. --output-path=<path> for each output [optional]
   --output-node-properties              A list of keys that indicate what additional file attributes should be captured --output-node-properties=<name> of each property [optional]
   --stdout-file                         Filepath to store stdout [optional]
   --stderr-file                         Filepath to store stderr [optional]
   --platform-properties                 The platform requirements for the execution environment --platform-properties=<key>=<value>,<key>=... for each property [optional]
   --environment                         [Deprecated] Use --env instead [optional]
   --env                                 Environment variables to set in the running program's environment. To specify multiple ones, use `--env=K1=V1 --env=K2=V2 ...` [optional]
   --exec-timeout                        The timeout after which the execution of an Action should be killed --exec-timeout=<seconds> [optional]
   --priority                            The priority (relative importance) of this action. A priority of 0 means the *default* priority --priority=<priority> [optional]
   --skip-cache-lookup                   Flag indicating that the server should not check the cache when executing an action [optional]
   --do-not-cache                        Flag indicating that ActionResults should not be cached, and duplicate Action executions will not be merged [optional]
   --follow-symlinks                     Follow symlinks when building input trees. If enabled, each input cannot be circular. Enabled by default [optional]
   --do-not-follow-symlinks              Do not follow symlinks [optional]
   --correlated-invocations-id           An identifier to tie multiple tool invocations together. For example, runs of foo_test, bar_test and baz_test on a post-submit of a given patch. --correlated-invocations-id=<str> [optional]
   --tool-invocation-id                  An identifier that ties multiple actions together to a final result. For example, multiple actions are required to build and run foo_test. --tool-invocation-id=<str> [optional]
   --tool-name                           Additional string to append to the tool-name (trexe) sent in the metadata. Useful to append additional information about invocation e.x. python to indicate python was used--tool-name=<str> [optional]
   --tool-version                        Additional string to append to the tool-version (e.x. 0.0.1) sent in the metadata. Useful to append additional information about invocation e.x. 2.3.4 to append 2.3.4 (version of a package named intool-name) to the tool-version metadata--tool-version=<str> [optional]
   --salt                                An optional value to place the `Action` into a separate cache namespace from other instances having the same field contents--salt=<str> [optional]
   --wait                                Flag indicating that calls from trexe should be blocking.this is the default behavior [optional]
   --no-wait                             Flag indicating that calls from trexe should be async. Thiswill cause trexe to exit with code 0 upon successful job submission and write the operation id to `stdout` in plaintext (e.g. app-cr/6283a9c6-85b2-4b03-92bd) [optional]
   --operation                           Operation ID to lookup and download outputs from (to the  specified directory) if -d is set. [optional]
   --cancel                              Cancels the specified operation. Requires --operation. [optional]
   --result-metadata-file                The metadata of trexe execution in JSON will be written into this file [optional]
   --action-result-json                  The full action result in JSON will be written into this file  [optional]
   --cache-only                          If not already cached, execute the action in a local BuildBox runner, caching the result [optional]
   --buildbox-run                        Runner command for local execution [optional]
   --runner-arg                          Arguments to pass to buildbox-run for local execution
                                            This can be useful if the buildbox-run implementation you're using supports non-standard options [optional]
   --stream-logs                         Enables live streaming of the remote command's stdout/stderr to the local stdout/stderr. [optional]
   --log-level                           Log level (debug, error, info, trace, warning) [optional, default = "error"]
   --verbose                             Set log level to 'debug' [optional]
     Command to remote                   POSITIONAL [optional]
```


## TOML Config File

Use `--config-file=/path/to/config.toml` to specify options in a TOML file. Options specified in CLI will override ones from the config file.

For connection options, they will be overridden by the order of
1. common option in TOML `[connection]`
2. individual option in TOML `[connection.*]`
3. common option in CLI `--remote` ...
4. individual option in CLI `--*-remote` ...

### Example

Note: all options below are optional.

```toml
log-level = "error" # one of "trace", "debug", "warn", "info" or "error"
cache-only = true
buildbox-run = "buildbox-run-bubblewrap" # runner for cache-only mode
runner-args = ["--verbose"]
stream-logs = true

# cancel = true # to cancel remote operation
# operation = "operation-id" # to cancel or download remote execution
working-dir = "."
download-path = "out"
stdout-path = "tmp/stdout"
stderr-path = "tmp/stderr"

command = ["gcc", "-o", "hello", "hello.c"]

input-root-digest = "somehash/123"
output-path = ["build", "output"]
output-node-property = ["mtime"]
exec-timeout = 3600
do-not-cache = false
follow-symlinks = true
salt = "salt"
priority = 1
no-wait = true # To submit remote execution asynchronously

result-metadata-file = "metadata.json"
action-result-json = "result.json"

tool-name = "build-system"
tool-version = "1.2.3"
tool-invocation-id = "iid"
correlated-invocations-id = "ciid"

[[input-path]]
path = "./build.sh"
[[input-path]] # path remapping
local = "src"
remote = "remote/src"

[platform]
OSFamily = "linux"
ISA = "rv64g"
foo = ["bar", "baz"]

[environment]
FOO = "FOO"
BAR = "BAR"

# REAPI connections
[connection] # Default common connection setting
remote = "https://remote:50051"
instance = "dev"
retry-limit = 5
# other options similar to CLI are also available

[connection.exec] # Override exec connection
instance = "storage"
# ...
[connection.cas] # Override cas connection
min-throughput = "10M"
# ...
[connection.ac] # Override ac connection
# ...
[connection.logstream] # Override logstream connection
# ...
```
