#!/bin/bash

mkdir -p /tmp/upload/input1;

echo "#!/bin/bash
echo \"Compiling a hello world\"
mkdir -p output
/usr/bin/g++ hello_world.cpp -o output/hello_world
./output/hello_world
" > /tmp/upload/input1/hello.sh;

chmod +x /tmp/upload/input1/hello.sh;

echo "#include <iostream>

int main()
{
    std::cout << \"Sent from trexe: Hello World!\" << std::endl;;
    return 0;
}" > /tmp/upload/input1/hello_world.cpp;

OPERATION=$(/usr/local/bin/trexe --remote=http://controller:50051 --no-wait \
                --input-path=/tmp/upload/input1 --output-path=output \
                --result-metadata-file=/tmp/result1.json "./hello.sh");

sleep 5;

/usr/local/bin/trexe --operation="${OPERATION}" --no-wait --d=/home \
    --remote=http://controller:50051 --result-metadata-file=/tmp/result2.json \
    && test -f /tmp/result1.json \
    && test -f /tmp/result2.json
