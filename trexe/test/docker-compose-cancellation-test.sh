#!/bin/bash

sleep 10;

mkdir -p /tmp/upload/input1;

echo "/usr/bin/g++ -c /app/tests/cas/data/hello/hello_world.cpp -o /app/tests/cas/data/hello/hello_world.o" > /tmp/upload/input1/hello.sh;
echo "#include <iostream>

int main()
{
    std::cout << \"Hello World!\";
    return 0;
}" > /tmp/upload/input1/hello_world.cpp;

OPERATION=$(/usr/local/bin/trexe --remote=http://controller:50051 --no-wait /bin/sleep 100);

sleep 5;

/usr/local/bin/trexe --operation=${OPERATION} --cancel --remote=http://controller:50051
