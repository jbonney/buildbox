#!/bin/bash

# wait for buildgrid
sleep 10

mkdir -p /tmp/upload/input1

echo "#!/bin/bash" >/tmp/upload/input1/test.sh
echo "echo output" >>/tmp/upload/input1/test.sh
echo "echo error >&2" >>/tmp/upload/input1/test.sh
chmod +x /tmp/upload/input1/test.sh

/usr/local/bin/trexe --remote=http://controller:50051 --stdout-file=/tmp/stdout --stderr-file=/tmp/stderr --input-path=/tmp/upload/input1 "./test.sh"

out=$(</tmp/stdout)
err=$(</tmp/stderr)

if [[ "$out" != "output" ]]; then
    exit 1
fi
if [[ "$err" != "error" ]]; then
    exit 1
fi
