/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_TREXE_ACTIONBUILDER
#define INCLUDED_TREXE_ACTIONBUILDER

#include <map>
#include <memory>
#include <optional>
#include <set>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <buildboxcommon_casclient.h>
#include <buildboxcommon_cashash.h>
#include <buildboxcommon_merklize.h>
#include <buildboxcommon_protos.h>

#include <trexe_actiondata.h>
#include <trexe_executionoptions.h>

using namespace buildboxcommon;

namespace trexe {

ActionData
buildAction(std::shared_ptr<CASClient> casClient,
            const std::vector<std::string> &argv,
            const std::string &workingDir,
            const std::vector<InputPathOption> &inputPaths,
            const std::shared_ptr<Digest> inputRootDigest,
            const std::set<std::string> &outputPaths,
            const std::set<std::pair<std::string, std::string>> &platform,
            const std::map<std::string, std::string> &environment,
            const int &execTimeout, const bool doNotCache,
            const bool followSymlinks, const std::string &salt,
            const std::set<std::string> &outputNodeProperties);

} // namespace trexe

#endif
