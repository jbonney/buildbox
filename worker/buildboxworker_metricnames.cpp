// Copyright 2020 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <buildboxworker_metricnames.h>

namespace buildboxworker {

// Elapsed wall time of `download action` operations
const std::string MetricNames::TIMER_NAME_DOWNLOAD_ACTION =
    "buildboxworker.download_action";

// Elapsed wall time of `update botsession` operations
const std::string MetricNames::TIMER_NAME_UPDATE_BOTSESSION =
    "buildboxworker.update_botsession";

// Elapsed wall time of `create botsession` operations
const std::string MetricNames::TIMER_NAME_CREATE_BOTSESSION =
    "buildboxworker.create_botsession";

// Elapsed wall time of `execute action` operations
const std::string MetricNames::TIMER_NAME_EXECUTE_ACTION =
    "buildboxworker.execute_action";

} // namespace buildboxworker
