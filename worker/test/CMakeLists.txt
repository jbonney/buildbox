include(${CMAKE_SOURCE_DIR}/cmake/BuildboxGTestSetup.cmake)

include_directories(. ..)

macro(add_worker_test TEST_NAME TEST_SOURCE)
    # Create a separate test executable per test source.
    add_executable(worker_${TEST_NAME} ${TEST_SOURCE})
    target_precompile_headers(worker_${TEST_NAME} REUSE_FROM common)

    # This allows us to pass an optional argument if the cwd for the test is not the default.
    set(ExtraMacroArgs ${ARGN})
    list(LENGTH ExtraMacroArgs NumExtraArgs)
    if(${NumExtraArgs} GREATER 0)
      list(GET ExtraMacroArgs 0 TEST_WORKING_DIRECTORY)
    else()
      set(TEST_WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
    endif()

    add_test(NAME worker_${TEST_NAME} COMMAND worker_${TEST_NAME} WORKING_DIRECTORY
             ${TEST_WORKING_DIRECTORY})

    target_link_libraries(worker_${TEST_NAME} PUBLIC buildboxworker ${GTEST_MAIN_TARGET} ${GMOCK_TARGET})
endmacro()

add_worker_test(botsessionutils_tests buildboxworker_botsessionutils.t.cpp)
add_worker_test(cmdlineparser_tests buildboxworker_cmdlineparser.t.cpp)
add_worker_test(executeactionutils_tests buildboxworker_executeactionutils.t.cpp)
add_worker_test(expiretime_tests buildboxworker_expiretime.t.cpp)
add_worker_test(tests buildboxworker_worker.t.cpp)
add_worker_test(logstream_debug_utils buildboxworker_logstreamdebugutils.t.cpp)
add_worker_test(actionutils_tests buildboxworker_actionutils.t.cpp ${CMAKE_CURRENT_SOURCE_DIR}/data)
add_worker_test(config_tests buildboxworker_config.t.cpp ${CMAKE_CURRENT_SOURCE_DIR}/data)

add_executable(worker_testrunner buildboxworker_testrunner.m.cpp)
target_precompile_headers(worker_testrunner REUSE_FROM common)
target_link_libraries(worker_testrunner common)

set_tests_properties(worker_tests PROPERTIES ENVIRONMENT
                     "BUILDBOX_RUN=${CMAKE_CURRENT_BINARY_DIR}/worker_testrunner")
